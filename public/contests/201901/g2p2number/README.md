Ани обича да пише числа, но спазва правилата – когато започне пише число само с еднакви цифри С и числото е с N на брой цифри. После към полученото число сумира с цифрата Х.

Съставете програма, която образува число и сумира като Ани. 

## Вход
На един ред на входа с въвеждат три стойности С, N и Х, разделени с интервал.

## Изход
Изведете получения резултат след сумирането.

## Ограничения
- 0 < N < 20
- С ≠ 0

## Примери

### Вход:
```
2 4 1 
```
### Изход: 
```
2223 
```

### Вход:
```
4 5 8
```
### Изход: 
```
44452
```