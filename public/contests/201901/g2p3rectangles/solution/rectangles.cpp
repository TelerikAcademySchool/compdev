#include <iostream>
#include <algorithm>
using namespace std;

int a[1000002], b[1000002], n;

int main()
{
    int i, j, pa, maxa, pb, maxb, r;
    cin>>n;
    for (i=0; i<n; i++)
    {
        cin>>a[i]>>b[i];
        if (a[i]>b[i])
        {
            r=a[i];
            a[i]=b[i];
            b[i]=r;
        }
    }
        
    pa=0; maxa=a[0];
    for (i=0; i<n; i++)
        if (a[i]>maxa)
        {
            maxa=a[i]; pa=i;
        }
    pb=0; maxb=b[0];
    for (i=0; i<n; i++)
        if (b[i]>maxb)
        {
            maxb=b[i]; pb=i;
        } 
    
   if (pa==pb) cout<<maxa<<" "<<maxb<<endl;
   else cout<<-1<<" "<<maxa<<" "<<maxb<<endl;
    return 0;
}