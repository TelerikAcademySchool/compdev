#include <iostream>
#include <algorithm>
using namespace std;
struct Re
{
    int nom, a, b;
};
Re m1[100002], m2[100002];
int n;
void read()
{
    int i, x, y;
    cin>>n;
    for (i=0; i<n; i++)
    {
        cin>>x>>y;
        if (x>y) swap(x, y);
        m1[i].nom=m2[i].nom=i;
        m1[i].a=m2[i].a=x;
        m1[i].b=m2[i].b=y;
        
    }
}
//---------------
bool cmp1(Re edno, Re dve)
{
    if (edno.a<dve.a) return true;
    if (edno.a==dve.a)
    {
        if (edno.b<dve.b) return true;
        if (edno.b==dve.b) return edno.nom<dve.nom;
        else return false;
    }
    else return false;
}
bool cmp2(Re edno, Re dve)
{
    if (edno.b<dve.b) return true;
    if (edno.b==dve.b)
    {
        if (edno.a<dve.a) return true;
        if (edno.a==dve.a) return edno.nom<dve.nom;
        else return false;
    }
    else return false;
}
//---------------------
void print(Re f[])
{
    for (int i=0; i<n; i++)
        cout<<f[i].a<<" "<<f[i].b<<" -> "<<f[i].nom<<endl;
}
int main()
{
    int i, j, k;
    read();
    sort(m1, m1+n, cmp1);
    sort(m2, m2+n, cmp2);
    /*print(m1);
    cout<<"---------------"<<endl;
    print(m2);
    cout<<"---------------"<<endl<<endl;*/
    if (m1[n-1].a==m2[n-1].a && m1[n-1].b==m2[n-1].b) cout<<m1[n-1].a<<" "<<m1[n-1].b<<endl;
    else 
    {
        cout<<-1<<" "<<m1[n-1].a<<" "<<m2[n-1].b<<endl;
    }
    return 0;
}