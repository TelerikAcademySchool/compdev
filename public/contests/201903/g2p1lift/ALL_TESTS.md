№ | Вход | Изход
--- | --- | --- 
1 | 45 1 2 8 <br/> 10 5 | 36.6 <br/> 3 1 2
2 | 15 0.75 15 42 <br/> 25 21 | 10.125 <br/> 0 0 11
3 | 29 3 26 41 <br/> 43 32 | 79.2 <br/> 1 1 5
4 | 35 0.29 16 32 <br/> 80 20 | 8.294 <br/> 0 0 9
5 | 51 2.35 3 26 <br/> 51 9 | 101.99 <br/> 2 0 0
6 | 61 7.13 29 30 <br/> 85 31 | 349.37 <br/> 4 0 10
7 | 64 36.33 35 56 <br/> 6 3 | 1918.22 <br/> 319 1 2
8 | 73 31.11 10 91 <br/> 82 15 | 2053.26 <br/> 25 0 4
9 | 116 40 49 53 <br/> 99 15 | 3792 <br/> 38 2 0
10 | 1000 12.19 499 500 <br/> 100 13 | 9752 <br/> 97 4 0
11 | 1000 46.48 499 500 <br/> 100 13 | 37184 <br/> 371 6 6
12 | 10 0.1 15 50 <br/> 20 17 | 1 <br/> 0 0 1
13 | 100 1.01 15 100 <br/> 200 2 | 80.8 <br/> 0 40 1
14 | 16 2 1 250 <br/> 40 32 | 28.8 <br/> 0 0 29
