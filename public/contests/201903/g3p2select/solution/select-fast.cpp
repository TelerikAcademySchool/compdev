#include <iostream>
using namespace std;
bool pr[1000000];
int a[1000000], n, k, maxa;
void prime()
{
    int i, j;
    for (i=2; i<=maxa; i++) pr[i]=true;
    for (i=4; i<=maxa; i=i+2) pr[i]=false;
    for (i=3; i<=maxa; i=i+2)
    {
        if (pr[i]==false) continue;
        for (j=i*i; j<=maxa; j=j+i) pr[j]=false;
    }
}
void read()
{
    cin>>n>>k;
    cin>>a[0]; maxa=a[0];
    for (int i=1; i<n; i++)
    {
        cin>>a[i];
        if (a[i]>maxa) maxa=a[i];
    }
}
int main()
{
    int br=0, x, y, maxbr=0, i;
    read();
    prime();
    for (i=0; i<k; i++)
    {
        x=a[i];
        if (pr[x]==true) br++;
    }
    maxbr=br;
    for (i=1; i<n-k; i++)
    {
        x=a[i-1];
        y=a[i+k-1];
        if (pr[x]==pr[y]) continue;
        if (pr[x]==true && pr[y]==false) br--;
        if (pr[x]==false && pr[y]==true) br++;
        if (br>maxbr) maxbr=br;
    }
    cout<<maxbr<<endl;
    return 0;
}