Вики си записва ``N`` на брой естествени числа в дълга последователност. След това трябва да огради ``K`` на брой последователни числа, като се стреми в заграденото място да има най-много прости числа.  

Съставете програма **select**, която определя какъв е най-големият брой прости числа в ``К``<sup>те</sup> последователни.

## Вход
На първия ред на входа се въвежда ``N`` и ``K``.  
На втория ред се въвеждат ``N`` на брой числа ``Ai``. 

## Изход
Изведете най-голямия брой прости числа от ``K``<sup>те</sup> последователни. 
 
## Ограничения
* ``1`` <= ``К`` <= ``N`` <= ``1 000 000``
* ``2`` <= ``Аi`` <= ``1 000 000``

## Пример

### Вход:
```
5 10 15 10 8 0
```
### Изход: 
```
1
2 50
```

### Вход:
```
10 0 14 0 12 0 
```
### Изход: 
```
0
2 0
```

### Вход:
```
12 0 16 30 11 30 
```
### Изход: 
```
1
0 30
```