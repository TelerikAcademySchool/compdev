В една квадратна таблица с размер `N*N` са записани цели числа. 
Разглеждаме нейна квадратна „подтаблица“ с размер `K*K`. 

Съставете програма **matrix**, която пресмята колко най-много нули има в някоя от подтаблиците `K*K`.


## Вход
На първия ред на входа се въвеждат `N` и `K`, разделени с интервал.
На следващите `N` реда се въвеждат по `N` числа, разделени с интервал.

## Изход
Изведете търсения брой нули. 
 
## Ограничения
* `2` <= `N` <= `5 000`  
* `1` <= `К` <= `5 000`
* `K` < `N`

## Пример

### Вход:
```
4 2
1 0 6 0
0 0 9 2
0 1 0 5
0 4 0 0
```
### Изход: 
```
3
```