№ | Вход | Изход
--- | --- | --- 
1 | ab6K3 <br/> 2 4 | 3b6Ka <br/> 3baK6 <br/> 6b3Ka <br/> 6baK3 <br/> ab3K6 <br/> ab6K3
2 | VJGE7 <br/> 2 3 | 7JGEV <br/> 7JGVE <br/> EJG7V <br/> EJGV7 <br/> VJG7E <br/> VJGE7
3 | BmsVh <br/> 1 5 | BVmsh <br/> BVsmh <br/> BmVsh <br/> BmsVh <br/> BsVmh <br/> BsmVh
4 | f5qSM <br/> 1 2 | f5MSq <br/> f5MqS <br/> f5SMq <br/> f5SqM <br/> f5qMS <br/> f5qSM
5 | 8MoPv <br/> 4 5 | 8MoPv <br/> 8oMPv <br/> M8oPv <br/> Mo8Pv <br/> o8MPv <br/> oM8Pv
6 | pqcTE <br/> 3 4 | EpcTq <br/> EqcTp <br/> pEcTq <br/> pqcTE <br/> qEcTp <br/> qpcTE
7 | 25wSk <br/> 4 5 | 25wSk <br/> 2w5Sk <br/> 52wSk <br/> 5w2Sk <br/> w25Sk <br/> w52Sk
8 | Lwp2y <br/> 2 1 | Lw2py <br/> Lw2yp <br/> Lwp2y <br/> Lwpy2 <br/> Lwy2p <br/> Lwyp2
9 | Pv7En <br/> 1 4 | P7nEv <br/> P7vEn <br/> Pn7Ev <br/> PnvE7 <br/> Pv7En <br/> PvnE7
10 | Pv7En <br/> 4 1 | P7nEv <br/> P7vEn <br/> Pn7Ev <br/> PnvE7 <br/> Pv7En <br/> PvnE7
11 | 31694 <br/> 5 1 | 31694 <br/> 31964 <br/> 36194 <br/> 36914 <br/> 39164 <br/> 39614
