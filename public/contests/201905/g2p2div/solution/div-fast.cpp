/*
Търсенето число има вида x = a^2*b^2, a != b, x > n
От тук следва, че a*b > floor(sqrt(n))
Нека означим m = a*b
Остава за съответното m да се провери дали може да бъде
представено като произведение на две различни числа, по-големи от 1
Ако е възможно, тогава m^2 е отговор на задачата
Сложност sqrt(n) * k, където k e броят на проверките, които правим
Груба оценка: k < 10
*/

#include <iostream>
#include <cmath>
using namespace std;

bool check(int x)
{
    for (int i = 2; i * i < x; ++ i)
    {
        if (x % i == 0 and x % i != i) return true;
    }
    return false;
}

int main()
{
    int n;
    cin >> n;

    int m = sqrt(n);
    while (m * m <= n) m++;

    int cnt = 0;
    while (cnt < 3)
    {
        if (check(m))
        {
            cout << m * m << endl;
            cnt++;
        }
        m++;
    }

    return 0;
}
