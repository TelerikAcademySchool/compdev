Всяко число има поне два делителя – `1` и себе си.   
Някои делители са „нахални“ и се появяват по два пъти. Единицата не я считаме за „нахален“ делител.

Съставете програма **divK**, която да намира първите `3` числа, по-големи от `N`, които са съставени от точно два различни "нахални" делителя.


## Вход
На първия ред на входа се въвеждат `N` и `K`, разделени с интервал.

## Изход
На три отделни реда изведете трите търсени числа. 
 
## Ограничения
* `2` <= `N` <= `200 000`  

## Пример

### Вход:
```
10
```
### Изход: 
```
36
64
100
```

> *Пояснение*: Никое от следните числа не влиза в отговора, защото:  
> - при `12=2*2*3` има два делителя 2-ки и още един 3-ка.
> - при `16=2*2*2*2=4*4` двойката се повтаря повече от два пъти и затова тя не е „нахален“ делител (тя е супер-нахален делител)    
> - при `25=5*5` има само един, а не два "нахални" делителя 