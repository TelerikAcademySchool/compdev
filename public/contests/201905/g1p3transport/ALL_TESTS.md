№ | Вход | Изход
--- | --- | --- 
1 | 10 AF <br/> 25 EU <br/> 105 EC | 11
2 | 30 AA <br/> 53 AU <br/> 17 AA | 22
3 | 20 EC <br/> 35 AF <br/> 8 AA | 7
4 | 55 AF <br/> 52 EU <br/> 59 AF | 19
5 | 84 AU <br/> 60 AF <br/> 90 AM | 42
6 | 13 EU <br/> 51 AU <br/> 44 AF | 22
7 | 200 AU <br/> 300 EC <br/> 900 AM | 199
8 | 679 AA <br/> 10 AM <br/> 708 AM | 193
9 | 10000 AM <br/> 10000 AA <br/> 10000 EU | 3600
10 | 5020 EU <br/> 6051 EU <br/> 865 AU | 1155
11 | 6510 EU <br/> 1410 AA <br/> 8710 EC | 1303
12 | 10000 AM <br/> 10000 AM <br/> 10000 AM | 4350
