Фирма „ГТ - Гошо Транспорт“ доставя пратки по света и е разделила дестинациите на няколко зони. 

Цената на доставка се формира в зависимост от тежестта и дестинацията на товара - на всеки `10` започнати килограма от тежестта на пратката се заплаща сума в зависимост дестинацията. 

Означенията и цените на дестинациите са както следва:  
> `EU` – по `0.85` на всеки `10` килограма от пратката  
> `AM` – по `1.45`  
> `AF` – по `1.1`  
> `AA` – по `1.3`  
> `EC` – по `0.65`  
> `AU` – по `2.44`  

Сега „ГТ - Гошо Транспорт“ трябва да достави `3` пратки, като за всяка от тях знае дестинацията и тежестта.
 
Съставете програма **transport**, която пресмята колко ще е сумата за доставка на трите пратки.

## Вход
На всеки от трите реда се въвеждат данните за съответната доставка – тежестта `Т` и дестинацията.

## Изход
Изведете сумата за доставка на трите пратки.  
Ако сумата не е цяло число, я изведете закръглена до следващо цяло число. 
 
## Ограничения
* `1` <= `T` <= `10000` 

## Пример

### Вход:
```
10 AF
25 EU
105 EC
```
### Изход: 
```
11
```

### Вход:
```
30 AA
53 AU
17 AA
```
### Изход: 
```
22
```