№ | Вход | Изход
--- | --- | --- 
1 | 4 4 <br/> luka <br/> o#a# <br/> kula <br/> i#as | as
2 | 4 5 <br/> o#dnp <br/> zji#f <br/> v#d#a <br/> e#a## | dida
3 | 5 6 <br/> #tyt## <br/> zaaaaw <br/> #l#q#w <br/> zephz# <br/> #s#up# | tales
4 | 7 4 <br/> rykj <br/> xcm# <br/> zz#o <br/> #aaa <br/> #qzz <br/> f#a# <br/> yiai | aaa
5 | 8 8 <br/> wyhjkeor <br/> eo#qyggz <br/> cjpuelsd <br/> sgqjzwkr <br/> irbrva#i <br/> lxvswdip <br/> xqja#uga <br/> oulvhbes | cjpuelsd
6 | 10 15 <br/> #zzzzz#adobcxhc <br/> p##guvawbwopsic <br/> a#xzovd#kiclyht <br/> #igrsqowa#bklwg <br/> jjrfti##c#wfudg <br/> z#jfkq#uemjywuz <br/> wyp#adoniskkhtw <br/> xmpqf#dxaq#zjtb <br/> vxswmaveet#xnfq <br/> dtztyoawd##rclb | ado
7 | 15 15 <br/> tkys#tfoatosfdn <br/> pcjjhgeoyylnikz <br/> anvufh#b#hknkhf <br/> cacdlmeypccffje <br/> adulqbtwz#fzozz <br/> kzrzqznvqd#suqq <br/> ozrzixdvtyth#lz <br/> nqowmplsogyfeo# <br/> ixcuoug#asvmyvn <br/> aesqsgxfceehhjv <br/> i#tmhjajq#gksaa <br/> afc#qugtxgkl#pk <br/> zqe#efmlued#cob <br/> r#xgwdkdjdjl#al <br/> ogiknvoshkesos# | adulqbtwz
8 | 19 19 <br/> ##h#cng##x#d###d### <br/> ####f###p#####zdp#u <br/> niiv#p######vupzp#n <br/> p###f#####m#####t## <br/> #upfkt#jd###r####gn <br/> ##o####q##g#g#tuy## <br/> #ox##q#q##te#w#d#c# <br/> #svyq###e##p###o### <br/> k#chin###t##dciz#s# <br/> ld#j#ghjizm##xnhy## <br/> jei###nup#k###q#### <br/> vn##edg#####m#o###r <br/> s#pjul##mud######## <br/> #h###v######qv#x### <br/> #g##vp##i#t###r#p## <br/> #db##q#dm########## <br/> g###kxz###g##d###y# <br/> mltn###t#t#g##ff### <br/> r#h#######x#r##r#r# | cf
9 | 20 20 <br/> jd#jlgsr#hwptyhapyaj <br/> mh#obte#vaqj#g#gfihp <br/> #soucejumbxoqkl#iqmq <br/> lw#uprsn#i#dpxtob#gv <br/> semqved##mpjsxkjnc#a <br/> g#l#cscm#eydleepg#er <br/> abfwlrt#qbkpgfhmewb# <br/> #bohvbclmidra#lmzo#q <br/> phblbhw#ufuenolramhw <br/> geozvztnm#hrw##qmhyu <br/> wkkwbxcefgxjt#enn#g# <br/> ivwodgjardpdcbexxwt# <br/> #ezidvzcclaxvuf#ts#a <br/> urzuia#qborkuh#ptns# <br/> utsjcg#xzbstqmkwcaha <br/> tgzbxwddyppziphy#xxy <br/> gkiop#z##ffgyhwopojp <br/> nt#keewrcx#pwgeays#l <br/> pfuxbu#tz#edjq#xtwad <br/> eboam#j#abfwlrtkrtsr | abfwlrt
10 | 4 4 <br/> luka <br/> o#a# <br/> kula <br/> i#a# | kala
11 | 4 5 <br/> adaca <br/> da##b <br/> abb#b <br/> abbac | abb
