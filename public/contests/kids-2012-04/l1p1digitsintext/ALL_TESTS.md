№ | Вход | Изход
--- | --- | --- 
1 | D2h40go | 6
2 | aaaacd1 | 1
3 | a23 b23 | 10
4 |  | -1
5 | 9999987 | 60
6 | 4aaaa4a | 8
7 | 00ss010 | 1
8 | j3ls945 | 21
9 | aj39103 | 16
10 | 99s3ks8 | 29
11 | 0000000 | 0
