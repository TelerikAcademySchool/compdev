// task Family, with sort
#include <iostream>
#include <algorithm>
#define MAXN 10002
using namespace std;
int a[MAXN];
int main()
{
    int n, i, br = 1;
    cin >> n;
    for (i = 0; i < n; i++)
        cin >> a[i];
    sort(a, a + n);
    for (i = 0; i < n - 1; i++)
        if (a[i] != a[i + 1])
            br++;
    cout << br / 3 << endl;
    return 0;
}