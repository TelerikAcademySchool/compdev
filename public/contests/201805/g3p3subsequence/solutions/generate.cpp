#include <iostream>
#include <random>

using namespace std;

int main ()
{
    std::random_device rd;
    std::mt19937 mt(rd());
    std::uniform_real_distribution<double> dist(1.0, 1000.0);

    int n = dist(mt);
    cout << n << '\n';

    for (int i = 0; i < n; ++i)
    {
        cout << (int)dist(mt) << ' ';
    }
    cout << '\n';
    return 0;
}
