#include <iostream>
using namespace std;

int n;
int a[1024];
int ans[1024], as=0;
int dp[1024][1024];
int pr[1024][1024];
int f(int from, int to){
    if (dp[from][to]) return dp[from][to];
    if (to < from) return 0;
    if (from == to) return 1;
    if (a[from] == a[to]){
        pr[from][to] = 3;
        return dp[from][to] = f(from+1, to-1)+2;
    }
    int l = f(from, to-1), r = f(from+1, to);
    if (l < r){
        pr[from][to] = 2;
    }else{
        pr[from][to] = 1;
    }
    return dp[from][to] = max(l, r);
}

int main(){
    cin>>n;
    for (int i=0; i<n; ++i){
        cin>>a[i];
    }
    f(0, n-1);
    int from = 0, to = n-1;

    while (from < to){
        if (pr[from][to] == 3){
            ans[as++] = a[from];
            ++from; --to;
        }else if (pr[from][to] == 2) ++from;
        else --to;
    }
    for (int i=0; i<as; ++i) cout<<ans[i]<<" ";
    if (from == to) cout<<a[from]<<" ";
    for (int i=as-1; i>=0; --i) cout<<ans[i]<<" ";
    cout<<"\n";
    return 0;
}
