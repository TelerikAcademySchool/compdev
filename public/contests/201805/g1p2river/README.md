Гошо и Пешо хвърлят камъни в реката. За по-лесно ще си представяме реката като отсечка, в чието начало се намират двете деца. Гошо хвърля камък на **G**-тия метър от реката и камъкът образува окръжност с радиус **Rg** преди да изчезне. Пешо хвърля камък на **P**-тия метър от реката. Какъв най-малко трябва да е радиусът на окръжността, която образува камъкът на Пешо, за да се пресекат окръжностите на двете деца? За пресичане броим ако окръжностите имат поне една обща точка.

## Вход
- На първия ред на входа се въвеждат 3 числа - **G**, **Rg** и **P**.

## Изход
- На единствен ред изведете едно число - търсеният най-малък радиус. 
- Ако Пешо хвърли камъка си в окръжността на Гошо, изведете 0.

## Ограничения
- Всички числа на входа са цели неотрицателни числа, не по-големи от 100 000. 

## Примери 

### Вход
```
3 6 12
```
### Изход
```
3
```

