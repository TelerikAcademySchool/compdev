#include <iostream>
using namespace std;
int n, k;
int a[16];
int main(){
    cin>>n>>k;
    int nperm = 1;
    for (int i=0; i<n; ++i){
        a[i] = i;
        nperm *= i+1;
    }
    int ans=0;
    for (int i=0; i<nperm; ++i){
        int cnt=0;
        for (int j=0; j<n; ++j){
            if (a[j] != j) ++cnt;
        }
        if (cnt >= k) ++ans;

        //find next permutation
        int pos = n-2;
        for (; a[pos] > a[pos+1]; --pos);
        int pos2 = pos+1;
        for (; pos2<n && a[pos] < a[pos2]; ++pos2);
        swap(a[pos], a[pos2-1]);
        for (int j=pos+1; j<n-j+pos; ++j){
            swap(a[j], a[n-j+pos]);
        }
    }
    cout<<ans;
    return 0;
}
