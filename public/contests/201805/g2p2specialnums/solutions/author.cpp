#include <iostream>

using namespace std;

int primeNumbers[20] = {2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37};
int main ()
{
    int n, m;
    cin >> n >> m;

    int l = 0;
    while (l < 12 and primeNumbers[l] <= n)
    {
        ++l;
    }

    if (l < m)
    {
        cout << 0 << '\n';
        return 0;
    }

    int a = 1;
    for (int i = m + 1; i <= l; ++i)
    {
        a *= i;
    }

    int b = 1;
    for (int i = 2; i <= l - m; ++i)
    {
        b *= i;
    }

    cout << a / b << '\n';
    return 0;
}
