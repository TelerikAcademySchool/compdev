#include <iostream>
using namespace std;

int main(){
    int n, m, p=0;
    cin>>n>>m;
    for (int i=2; i<=n; ++i){
        bool pr=true;
        for (int j=2; j*j<=i; ++j){
            if (i%j == 0){
                pr=false;
                break;
            }
        }
        if (pr) ++p;
    }
    long long ans = 1;
    for (int i=0; i<m; ++i){
        ans*=p-i;
        ans/=i+1;
    }
    cout<<ans<<"\n";
    return 0;
}
