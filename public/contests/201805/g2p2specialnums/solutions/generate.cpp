#include <iostream>
#include <random>

using namespace std;

int primeNumbers[20] = {2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37};

int main ()
{
    std::random_device rd;
    std::mt19937 mt(rd());
    std::uniform_real_distribution<double> distN(1.0, 40.0);
    std::uniform_real_distribution<double> distM(1.0, 10.0);

    int n = distN(mt), m = distM(mt);
    while (true)
    {
        int l = 0;
        while (l < 12 and primeNumbers[l] < n)
        {
            ++l;
        }

        if (l >= m)
        {
            break;
        }
        n = distN(mt);
        m = distM(mt);
    }

    cout << n << ' ' << m << '\n';
    return 0;
}
