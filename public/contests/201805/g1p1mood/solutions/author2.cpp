#include <iostream>
using namespace std;

char text[1024];
int n;

int main(){
    cin>>n;
    cin>>text;
    int cnt=0;
    for (int i=0; i<n; ++i){
        if (text[i]>='a' && text[i]<='z') ++cnt;
        if (text[i]>='A' && text[i]<='Z') --cnt;
    }
    if (cnt>0) cout<<"happy\n";
    if (cnt==0) cout<<"neutral\n";
    if (cnt<0) cout<<"angry\n";
    return 0;
}
