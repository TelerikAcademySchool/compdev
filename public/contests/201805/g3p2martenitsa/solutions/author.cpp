#include <iostream>

int size;
char field[1024][1024];

bool check(int x, int y, int s) {
    if(s == 1) return true;

    // check if tl=br
    bool diff = false;
    for(int nx = x;nx < x + s/2;nx ++) {
        for(int ny = y;ny < y + s/2;ny ++) {
            if(field[nx][ny] != field[nx + s/2][ny + s/2]) {
                diff = true;
            }
        }
    }

    if(!diff) {
        return (check(x + s/2, y, s/2) and check(x, y + s/2, s/2));
    }

	return false;
}

int main() {

    int t;
    std::cin >> size >> t;
    for(int test = 0;test < t;test ++) {
        for(int y = 0;y < size;y ++) {
            for(int x = 0;x < size;x ++) {
                std::cin >> field[x][y];
            }
        }

        std::cout << check(0, 0, size);
    }
    std::cout << '\n';
    return 0;
}
