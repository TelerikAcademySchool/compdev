// Решението принадлежи на потребителя jasen_penchev в системата Judge.

#include<iostream>
using namespace std;

char a[1025][1025];

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(0); cout.tie(0);
    
    int s,n;
    cin >> s >> n;
    
    for(int i=0; i<n; i++)
    {
        for(int j=0; j<s; j++)
            for(int k=0; k<s; k++)
                cin >> a[j][k];
        
        bool t=true;        
        for(int j=0; j<s/2; j++)
            for(int k=0; k<s/2; k++)
                if(a[j][k]!=a[j+s/2][k+s/2]) t=false;
                
        if(!t) { cout << 0; continue;}
        
        for(int j=0; j<s/4; j++)
            for(int k=s/2; k<s/2+s/4; k++)
                if(a[j][k]!=a[j+s/4][k+s/4]) t=false;
        
        if(!t) { cout << 0; continue;}
        
        for(int j=s/2; j<s/2+s/4; j++)
            for(int k=0; k<s/4; k++)
                if(a[j][k]!=a[j+s/4][k+s/4]) t=false;
        
        if(!t) { cout << 0; continue;}
        
        cout << 1;
    }
    
    cout << endl;
    
    return 0;
}