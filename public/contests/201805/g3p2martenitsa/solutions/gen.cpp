#include <iostream>
#include <random>

int size;
char field[1024][1024];

std::default_random_engine eng(1);
std::uniform_int_distribution<char> dist('a', 'c');

void gen(int x, int y, int s) {
    if(s == 1) {
        field[x][y] = dist(eng);
        return;
    }

    for(int nx = x;nx < x + s/2;nx ++) {
        for(int ny = y;ny < y + s/2;ny ++) {
            field[nx][ny] = dist(eng);
        }
    }

    for(int nx = x;nx < x + s/2;nx ++) {
        for(int ny = y;ny < y + s/2;ny ++) {
            field[nx + s/2][ny + s/2] = field[nx][ny];
        }
    }


    gen(x + s/2, y, s/2);
    gen(x, y + s/2, s/2);
}
void mutate(int x, int y, int s) {
    if(s == 1) {
        return;
    }

    std::uniform_int_distribution<int> dx(x, x + s/2 - 1);
    std::uniform_int_distribution<int> dy(y, y + s/2 - 1);
    const int nx = dx(eng);
    const int ny = dy(eng);

	if(dist(eng) % 2) field[nx][ny] = dist(eng);
	else field[nx + s/2][ny + s/2]  = dist(eng);

    mutate(x + s/2, y, s/2);
    mutate(x, y + s/2, s/2);
}

int main() {
    std::random_device rd;
    eng.seed(rd());
    //std::cout << dist(eng) << std::endl;

    int t;
    std::cin >> size >> t;
    std::cout << size << ' ' << t << std::endl;
    for(int test = 0;test < t;test ++) {
        gen(0, 0, size);
        if(dist(eng) % 2) {
            mutate(0, 0, size);
        }
        for(int y = 0;y < size;y ++) {
            for(int x = 0;x < size;x ++) {
                std::cout << field[x][y];
            }
            std::cout << '\n';
        }
    }
    std::cout << '\n';
    return 0;
}
