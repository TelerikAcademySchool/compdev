#include <iostream>
using namespace std;
int main(){
    int a, b, c, n;
    cin>>a>>b>>c>>n;
    if (a > b) swap(a, b);
    if (a > c) swap(a, c);
    if (b > c) swap(b, c);
    if (n==1) cout<<2*(a*b + b*c + c*a)<<"\n";
    if (n==2) cout<<a*b*c<<"\n";
    if (n==3) cout<<a*b<<"\n";
    if (n==4) cout<<2*(b+c)<<"\n";
    return 0;
}
