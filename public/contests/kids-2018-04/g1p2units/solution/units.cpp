// Решението принадлежи на потребителя anisha в системата Judge.

#include <iostream>
using namespace std;

float countRow(int number, char preffix);

int main()
{
    int a, max_row = 0;
    float row_value, max_value = 0;
    char prefix;
    for(int i=0; i<3; i++){
        cin >> a >> prefix;
        row_value = countRow(a, prefix);
        if(max_value < row_value){
            max_value = row_value;
            max_row = i+1;
        }
    }
    cout << max_row << endl;
    return 0;
}

float countRow(int number, char preffix)
{
    float result = (float)number;
    if(preffix == 'c' || preffix == 'C')
    {
        result *= 0.01;
    }
    else if(preffix == 'd' || preffix == 'D')
    {
        result *= 0.1;
    }
    else if(preffix == 'h' || preffix == 'H')
    {
        result *= 100;
    }
    else if(preffix == 'k' || preffix == 'K')
    {
        result *= 1000;
    }
    else if(preffix == 'm' || preffix == 'M')
    {
        result *= 1000000;
    }

    return result;
}