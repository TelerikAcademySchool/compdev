// Решението принадлежи на потребителя daniel.stanchev в системата Judge.

#include<iostream>
#include<string>
#include<algorithm>
using namespace std;
typedef unsigned long long int ull;
ull x=0, y=0;
ull nod(ull a, ull b)
{
    ull r;
    while(b!=0)
    {
        r=a%b;
        a=b;
        b=r;
    }
    return a;
}
ull nok(ull a, ull b)
{
    return a*b/nod(a, b);
}
int main()
{
    int n;
    cin>>n;
    for(int i=0;i<n;i++)
    {
        ull x1, y1;
        cin>>x1>>y1;
        if(x==0 && y==0)
        {
            x=x1;
            y=y1;
        }
        else
        {
            ull nk=nok(y, y1);
            ull m1, m2;
            m1=nk/y;
            m2=nk/y1;
            x*=m1; x1*=m2;
            y=nk;
            x=x+x1;
            ull nd=nod(x, y);
            x/=nd; y/=nd;
        }
    }
    ull nd=nod(x, y);
    cout<<x/nd<<" "<<y/nd<<endl;
    return 0;
}