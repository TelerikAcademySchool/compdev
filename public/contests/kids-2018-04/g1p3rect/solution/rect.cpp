// Решението принадлежи на потребителя yanicas в системата Judge.

#include<iostream>
using namespace std;
long long fabs(long long a, long long b)
{
    if(a>b) return a-b;
    else return b-a;
}
int main()
{
    long long a, b, c, d;
    cin>>a>>b>>c>>d;
    if(a==b) cout<<fabs(c, d)<<endl;
    else if(a==c) cout<<fabs(b, d)<<endl;
    else if(a==d) cout<<fabs(b, c)<<endl;
    else if(b==c) cout<<fabs(a, d)<<endl;
    else if(b==d) cout<<fabs(a, c)<<endl;
    else if(d==c) cout<<fabs(a, b)<<endl;
    return 0;
}