// Решението принадлежи на потребителя yanicas в системата Judge.

#include<iostream>
#include<math.h>
using namespace std;
int main()
{
    long long n;
    cin>>n;
    int sum=0, mult=1;
    while(n)
    {
        sum+=n%10;
        mult*=(n%10);
        n/=10;
    }
    int br=0;
    for(int i=1;i<=sum;i++)
        if(sum%i==0 && mult%i==0)
        br++;
    cout<<br<<endl;
    for(int i=1;i<=sum;i++)
        if(sum%i==0 && mult%i==0)
            cout<<i<<" ";
    cout<<endl;
    return 0;
}