
// Решението принадлежи на потребителя anisha в системата Judge.

#include<iostream>
using namespace std;
int main()
{
    int n, a, b, c, biggest, smallest, counter=0;
    cin>>n;

    c=n%10;
    b=n/10%10;
    a=n/100%10;

    smallest = biggest = a;
    //find biggest
    if(biggest < b)
    {
        biggest = b;
    }
    if(biggest < c)
    {
        biggest = c;
    }
    //find smallest
    if(smallest > b)
    {
        smallest = b;
    }
    if(smallest > c)
    {
        smallest = c;
    }

    //count dividers
    if(a!=0 && n%a==0)
    {
        counter++;
    }
    if(b!=0 && n%b==0)
    {
        counter++;
    }
    if(c!=0 && n%c==0)
    {
        counter++;
    }
    //print output
    if(counter == 0)
    {
        cout << "No" << " " << smallest << endl;
    }
    else if(counter == 1)
    {
        cout << "One:" << " ";
        if(a!=0 && n%a==0)
        {
            cout << a << endl;
        }
        else if(b!=0 && n%b==0)
        {
            cout << b << endl;
        }
        else if(c!=0 && n%c==0)
        {
            cout << c << endl;
        }
    }
    else if(counter == 2)
    {
        cout << "Two:" << " ";
        if(a!=0 && n%a==0)
        {
            cout << a << " ";
        }
        if(b!=0 && n%b==0)
        {
            cout << b << " ";
        }
        if(c!=0 && n%c==0)
        {
            cout << c << " ";
        }
        cout << endl;
    }
    else if(counter == 3)
    {
        cout << "All" << " " << biggest << endl;
    }

    return 0;
}