#include <iostream>

using std::cin; using std::cout; using std::cerr; using std::endl;

int main() {
	long long n, answer = 0;
	cin >> n;
	while(n != 1) {
		if(n % 2 == 0) n = n / 2;
		else           n = n*3 + 1;
		//cout << " -> " << n;
		answer ++;
	}
	//cout << endl;
	cout << answer << endl;
	return 0;
}

