#include <iostream>

using std::cin; using std::cout; using std::cerr; using std::endl;

int main() {
	long long total, underage;
	cin >> total >> underage;

	long long overage = total - underage;
	cout << overage * overage << endl;

	return 0;
}

