#include <iostream>
using namespace std;
int main() {
	int a, b, c, d;
	cin >> a >> b >> c >> d;
	cout << (b < c and c < d) << ' '
		 << (a > b and b > c) << ' '
		 << ((a == b) or (b == c) or (c == d)) << ' '
		 << (a >= d) << endl;
	return 0;
}

