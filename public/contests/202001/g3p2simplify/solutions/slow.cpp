/*Това решение не дава максимален брой точки, тъй като някои от тестовете не минават заради просрочено време*/

#include <iostream>
#include <algorithm>
#include <vector>

using std::cin; using std::cout; using std::cerr; using std::endl;

int arr[1004];
int sum(int from, int to) {
	int ans = 0;
	for(int i = from;i <= to;i ++) ans += arr[i];
	return ans;
}

int main() {
	int n;
	cin >> n;
	for(int i = 0;i < n;i ++) {
		cin >> arr[i];
	}

	std::vector<std::pair<int, int>> ans;

	for(int i = 0;i < n;i ++) {
		for(int j = i;j < n;j ++) {
			const int len = j - i + 1;
			if(sum(i, j) == 0) ans.push_back({-len, i});
		}
	}

	std::sort(ans.begin(), ans.end());

	for(auto a : ans) {
		std::cout << a.second << ' ' << a.second - a.first -1 << endl;
	}
	return 0;
}

