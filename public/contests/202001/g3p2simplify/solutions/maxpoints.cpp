/*Това решение е взето от участник в предишно състезание, решил задачата в системата Judge.*/

#include <iostream>
#include <algorithm>
using namespace std;

struct p
{
int p1;
int p2;
int d;
}a[1001];

bool comp( p x , p y )
{
    if(x.d>y.d)return true;
    if(x.d==y.d && x.p1<y.p1)return true;
    return false;
}

int b[1001] ;
int main()
{

int n;
cin>>n;
for(int i=0;i<n;i++)
    cin>>b[i];

int j=0;
for(int i=0;i<n;i++)
{
    int sb=b[i];
    if(sb==0)
    {
        a[j].p1=i;
        a[j].p2=i;
        a[j].d=0;
        j++;
    }
    for(int g=i+1;g<n;g++)
    {
        sb=sb+b[g];
        if(sb==0)
        {
            a[j].p1=i;
            a[j].p2=g;
            a[j].d=g-i;
            j++;

        }
    }
}
sort(a,a+j,comp);

for(int i=0;i<j;i++)
    cout<<a[i].p1<<" "<<a[i].p2<<endl;
return 0;
}