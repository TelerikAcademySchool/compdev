#include <iostream>

using std::cin; using std::cout; using std::cerr; using std::endl;

int main() {
	std::string inp, out;

	cin >> inp;

	for(int i = 0;i < inp.size() - 1;i ++) {
		const char smaller = std::min(inp[i], inp[i+1]);
		const char bigger =  std::max(inp[i], inp[i+1]);
		if( (smaller - 'a') == ('z' - bigger) ) {
			out += '(';
			out += inp[i];
			out += inp[i+1];
			out += ')';
			i ++; // skip the check for the next letter
		} else {
			out += inp[i];
		}
	}

	cout << out << endl;
	return 0;
}

