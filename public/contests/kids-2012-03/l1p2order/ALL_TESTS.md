№ | Вход | Изход
--- | --- | --- 
1 | 3 4 5 6 | Ascending
2 | 4 2 3 4 | Mixed
3 | 1 2 3 10 | Ascending
4 | 10 8 7 5 | Descending
5 | 21 20 13 10 | Descending
6 | 53 12 10 34 | Mixed
7 | 10 23 43 12 | Mixed
8 | 10 91 92 90 | Mixed
9 | 44 32 31 30 | Descending
10 | 90 88 82 81 | Descending
11 | 5 34 56 90 | Ascending
