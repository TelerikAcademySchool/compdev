№ | Вход | Изход
--- | --- | --- 
1 | aabb | Yes
2 | abcd | 1 3
3 | zzcd | 0 4
4 | aaab | 3 1
5 | bbba | 1 3
6 | abba | Yes
7 | abab | Yes
8 | naon | Yes
9 | uiby | 3 1
10 | yyuu | 4 0
11 | ooii | 4 0
