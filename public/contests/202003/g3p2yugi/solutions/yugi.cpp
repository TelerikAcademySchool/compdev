#include <iostream>
int main() {
	int N, important;
	std::cin >> N >> important;
	int extracted[10000];
	important --;
	for(int i = 0;i < N;i ++) {
		int params[3];
		std::cin >> params[0] >> params[1] >> params[2];
		extracted[i] = params[important];	
	}

	// solution without sort
	int max = -1;
	int maxA = 0, maxB = 0;
	for(int a = 0;a < N;a ++) {
		for(int b = a + 1;b < N;b ++) {
			if(extracted[a] + extracted[b] > max) {
				max = extracted[a] + extracted[b];
				maxA = a;
				maxB = b;
			}
		}
	}

	// NOT part of solution - check if test is correct
	{

	int cnt = 0;
	for(int a = 0;a < N;a ++) {
		for(int b = a + 1;b < N;b ++) {
			cnt += extracted[a] + extracted[b] == max;
		}
	}
	if(cnt > 1) return 1;

	}

	std::cout << maxA+1 << ' ' << maxB+1 << std::endl;
	return 0;
}