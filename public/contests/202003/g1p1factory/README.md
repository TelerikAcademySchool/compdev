# Поточна линия

Наети сте от компания, която произвежда и тества камери и други устройства за сигурност. За да бъде тествана една камера, тя се пуска по поточна линия и преминава през различни тестове. На края на поточната линия има машина, коятo трябва да прецени какво да се случва с тестваното устройство, като вземе предвид всички тестове.

Задачата ви е да програмирате тази машина! 

Хардуерните инженери са ви дали диаграма, която вашата програма трябва да следва. Според три входни параметъра **Q1**, **Q2**, **Q3** програмата трябва да отпечата един от 4 възможни низа. Всеки от параметрите **Q1**, **Q2** и **Q3** има стойност `0` (Не) или `1` (Да).

![flowchart](https://i.imgur.com/MfQYazB.png)

# Формат вход / изход

На единствения ред на входа ще има 3 цифри, разделени с интервал - отговорите на Q1, Q2 и Q3.

На изхода изведете правилният според тези параметри низ.

# Пример

### Вход
```
0 1 1
```
### Изход
```
rekalibraciq na sensora
```
