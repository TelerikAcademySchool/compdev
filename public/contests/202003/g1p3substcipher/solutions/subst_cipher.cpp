#include <iostream>

int main() {
	std::string key, plaintext;
	std::cin >> key >> plaintext;
	for(auto c : plaintext) {
		if(c == '_') std::cout << ' ';
		else         std::cout << key[c - 'a'];
	}
}