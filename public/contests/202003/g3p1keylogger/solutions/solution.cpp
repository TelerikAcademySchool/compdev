#include <iostream>

int main() {
	int N, stack[1000], ptr = 0;
	std::cin >> N;
	for(int i = 0;i < N;i ++) {
		int x;
		std::cin >> x;
		if(x == 0) {
			if(ptr == 0) return -1;
			ptr --;
		} else {
			stack[ptr ++] = x;
		}
	}

	for(int i = 0;i < ptr;i ++) std::cout << stack[i] << ' ';
	std::cout << std::endl;
	return 0;
}