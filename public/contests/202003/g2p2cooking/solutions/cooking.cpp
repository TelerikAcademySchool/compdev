#include <iostream>
int main() {
	int N, max_left = -1, min_right = 1e9;
	std::cin >> N;
	for(int i = 0;i < N;i ++) {
		int a, b;
		std::cin >> a >> b;
		max_left  = std::max(max_left, a);
		min_right = std::min(min_right, b);
	}

	if(max_left > min_right) std::cout << "-1\n";
	else std::cout << max_left << std::endl;
	return 0;
}