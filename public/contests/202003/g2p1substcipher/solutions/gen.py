import random, string, os

def gen_key():
	key = list(string.ascii_lowercase)
	random.shuffle(key)
	return ''.join(key)

def is_word_valid(word):
	for char in word:
		if char not in string.ascii_lowercase: return False

	return True

def gen_plaintext():
	all_words = [w.strip() for w in open('/usr/share/dict/words').readlines()]
	good_words = list(filter(is_word_valid, all_words))
	word_count = random.randint(10, 1000)
	return '_'.join(random.choices(good_words, k = word_count))

def write_test(id, points, in_contents, out_contents = None):
	yml.write(f' - {{in: test.{id}.in.txt, out: test.{id}.out.txt, points: {points}}}\n')

	open(f'tests/test.{id}.in.txt', 'w').write(in_contents)

	if out_contents is None:
		os.system(f'./solution < tests/test.{id}.in.txt > tests/test.{id}.out.txt')
	else:
		open(f'tests/test.{id}.out.txt', 'w').write(out_contents)


try: os.mkdir('tests/')
except FileExistsError: pass

yml = open('tests/init.yml', 'w')
yml.write('test_cases:\n')

print('compiling...')
os.system('g++ -std=c++17 -O2 solutions/subst_cipher.cpp -o solution')
print('done compiling')

for tnum in range(1, 20+1):
	in_contents = f'{gen_key()}\n{gen_plaintext()}\n'
	write_test(id = f'{tnum:03d}', points = 1, in_contents = in_contents, out_contents = None)
	print(f'done test {tnum}')
