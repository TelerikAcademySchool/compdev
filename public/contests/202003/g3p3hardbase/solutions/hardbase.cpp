#include <iostream>
#include <algorithm>
#include <random>

std::string sym = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
long long revsym[256];

std::string tobase(long long number, long long base) {
	std::string out = "";
	while(number > 0) {
		out += sym[number % base];
		number /= base;
	}
	std::reverse(out.begin(), out.end());
	return out;
}

long long frombase(std::string str, long long base) {
	long long mul = 1;
	long long num = 0;
	for(int i = str.size() - 1;i >= 0;i --) {
		num += revsym[str[i]] * mul;
		mul *= base;
	}
	return num;
}

void add1(std::string &str, long long base) {
	for(int i = str.size() - 1;i >= 0;i --) {
		int cv = revsym[str[i]] + 1;
		if(cv < base) {
			str[i] = sym[cv];
			return;
		}
		str[i] = sym[0];
	}
	str = sym[1] + str;
}

int main() {
	for(int i = 0;i < sym.size();i ++) revsym[sym[i]] = i;

	long long base, start;
	std::string needle;
	std::cin >> needle >> base >> start;

	std::string curr = tobase(start, base);

	while(true) {
		if(curr.find(needle) != std::string::npos) {
			std::cout << frombase(curr, base) << std::endl;
			break;
		}

		add1(curr, base);
	}
	return 0;
}
