import random, string, os

yml = None
def prepare():
	try: os.mkdir('tests/')
	except FileExistsError: pass

	global yml
	yml = open('tests/init.yml', 'w')
	yml.write('test_cases:\n')

	print('compiling...')
	os.system('g++ -std=c++17 -O2 solutions/hardbase.cpp -o solution')
	os.system('g++ -std=c++17 -O2 solutions/hardbase_slow.cpp -o solution_slow')
	print('done compiling')


def write_test(id, points, in_contents, out_contents = None):
	yml.write(f' - {{in: test.{id}.in.txt, out: test.{id}.out.txt, points: {points}}}\n')

	open(f'tests/test.{id}.in.txt', 'w').write(in_contents)

	if out_contents is None:
		os.system(f'./solution < tests/test.{id}.in.txt > tests/test.{id}.out.txt')
	else:
		open(f'tests/test.{id}.out.txt', 'w').write(out_contents)

def extract_tests_from_statement(fname = 'README.md'):
	statement = open('README.md').readlines()
	mode = 'nothing'
	inputs = []
	outputs = []
	curr_text = ''

	for line in statement:
		if mode == 'nothing':
			if line == '### Вход\n':
				mode = 'expect_begin'
				what = inputs
				curr_text = ''
				continue
			if line == '### Изход\n':
				mode = 'expect_begin'
				what = outputs
				curr_text = ''
				continue

		if mode == 'expect_begin' and line == '```\n':
			mode = 'in_text'
			continue

		if mode == 'in_text':
			if line == '```\n':
				mode = 'nothing'
				what.append(curr_text)
				continue
			curr_text += line

	if len(inputs) != len(outputs):
		raise Exception('Different number of inputs and outputs in statement')

	return zip(inputs, outputs)

def make_example_tests():
	for tnum, (inp, out) in enumerate(extract_tests_from_statement()):
		write_test(id = f'000.{tnum+1:03d}', points = 0, in_contents = inp, out_contents = out)
		print(f'done example test {tnum+1}')

def gen_input(big):
	MAX = 2**62 if big else 2**30

	# https://stackoverflow.com/questions/2267362/how-to-convert-an-integer-in-any-base-to-a-string
	def baseN(num,b,numerals="0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ"):
	    return ((num == 0) and numerals[0]) or (baseN(num // b, b, numerals).lstrip(numerals[0]) + numerals[num % b])

	def random_part_of_string(str):
		A = random.randint(0, len(str) - 1)
		B = random.randint(0, len(str) - 1)
		A, B = min(A, B), max(A, B)
		if len(str) == 1 or A == B: return str
		return str[A:B]

	initial = random.randint(1, MAX)
	if big: offset = random.randint(5e6, 1e7)
	else  : offset = random.randint(0, 5e4)
	base = random.randint(2, 36)
	inbase = baseN(initial+offset, base)
	out = f'{random_part_of_string(inbase)} {base} {initial}\n'
	return out

def make_real_tests():
	for tnum in range(1, 50+1):
		in_contents = gen_input(tnum > 20)

		write_test(id = f'{tnum:03d}', points = 1, in_contents = in_contents, out_contents = None)
		print(f'done real test {tnum}')

	# confirm tests are fine
	os.system("""
		for f in tests/*.in.txt; do 
			treal=$(time -f "%U" ./solution < $f 2>&1 1>/dev/null);
			tslow=$(time -f "%U" ./solution_slow < $f 2>&1 1>/dev/null);
			printf "[%s] real/slow: %s/%s sec.\n" $f $treal $tslow
		done""")

if __name__ == '__main__':
	prepare()
	make_example_tests()
	make_real_tests()