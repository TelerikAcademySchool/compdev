#include <iostream>
#include <algorithm>
#include <random>

std::string sym = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
std::string inbase(long long number, long long base) {
	std::string out = "";
	while(number > 0) {
		out += sym[number % base];
		number /= base;
	}
	std::reverse(out.begin(), out.end());
	return out;
}

int main() {
	long long base, start;
	std::string needle;
	std::cin >> needle >> base >> start;

	for(long long number = start;;number ++) {
		std::string numinbase = inbase(number, base);

		if(numinbase.find(needle) != std::string::npos) {
			//std::cout << number << '(' << base << ")=" << numinbase << std::endl;
			std::cout << number << std::endl;
			break;
		}
	}
	return 0;
}
