#include <iostream>
int main() {
	long long width, height, col, row;
	std::cin >> width >> height >> col >> row;
	std::cout << (row-1) * width + col << std::endl;
	return 0;
}