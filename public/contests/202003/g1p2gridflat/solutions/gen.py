import random, string, os

yml = None
def prepare():
	try: os.mkdir('tests/')
	except FileExistsError: pass

	global yml
	yml = open('tests/init.yml', 'w')
	yml.write('test_cases:\n')

	print('compiling...')
	os.system('g++ -std=c++17 -O2 solutions/gridflat.cpp -o solution')
	print('done compiling')


def write_test(id, points, in_contents, out_contents = None):
	yml.write(f' - {{in: test.{id}.in.txt, out: test.{id}.out.txt, points: {points}}}\n')

	open(f'tests/test.{id}.in.txt', 'w').write(in_contents)

	if out_contents is None:
		os.system(f'./solution < tests/test.{id}.in.txt > tests/test.{id}.out.txt')
	else:
		open(f'tests/test.{id}.out.txt', 'w').write(out_contents)

def extract_tests_from_statement(fname = 'README.md'):
	statement = open('README.md').readlines()
	mode = 'nothing'
	inputs = []
	outputs = []
	curr_text = ''

	for line in statement:
		if mode == 'nothing':
			if line == '### Вход\n':
				mode = 'expect_begin'
				what = inputs
				curr_text = ''
				continue
			if line == '### Изход\n':
				mode = 'expect_begin'
				what = outputs
				curr_text = ''
				continue

		if mode == 'expect_begin' and line == '```\n':
			mode = 'in_text'
			continue

		if mode == 'in_text':
			if line == '```\n':
				mode = 'nothing'
				what.append(curr_text)
				continue
			curr_text += line

	if len(inputs) != len(outputs):
		raise Exception('Different number of inputs and outputs in statement')

	return zip(inputs, outputs)

def make_example_tests():
	for tnum, (inp, out) in enumerate(extract_tests_from_statement()):
		write_test(id = f'000.{tnum+1:03d}', points = 0, in_contents = inp, out_contents = out)
		print(f'done example test {tnum+1}')

def gen_test(big):
	if big:
		width  = random.randint(1e7, 1e8)
		height = random.randint(1e7, 1e8)
	else:
		width  = random.randint(1, 1e3)
		height = random.randint(1, 1e3)

	col = random.randint(1, width)
	row = random.randint(1, height)
	return f'{width} {height} {col} {row}\n'

def make_real_tests():
	for tnum in range(1, 20+1):
		in_contents = gen_test(tnum > 10)

		write_test(id = f'{tnum:03d}', points = 1, in_contents = in_contents, out_contents = None)
		print(f'done real test {tnum}')

if __name__ == '__main__':
	prepare()
	make_example_tests()
	make_real_tests()