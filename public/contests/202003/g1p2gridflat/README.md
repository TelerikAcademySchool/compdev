# Игра на пиксели

В софтуерите за обработка на изображения, много често се налага пикселите в картинка да бъдат номерирани по начина показан отдолу:

![table](https://i.imgur.com/q7ClwTL.png)

Пикселът в най-левия горен ъгъл започва с номер едно и номерата се увеличават надясно. След края на реда се минава на следващият ред. Показаният на изображението пиксел е осмият пиксел на третия ред и има номер 30.

Напишете програма, която по дадени широчина и височина на картинката, както и колоната и реда на някой пиксел от нея, казва какъв номер ще получи той по този алгоритъм.

# Формат вход / изход

На единствения ред от входа има 4 числа. Първите две са широчина и височина на картинка, а следващите две - колона и ред на някой пиксел от картинката.

На един ред изведете номера на този пиксел.

# Ограничения
Всички числа на входа са до `100 000 000`.  
**В половината от тестовете, всички числа на входа са до `1000`.**

# Пример

### Вход
```
11 8 8 3
```
### Изход
```
30
```
### Пояснение
Вижте картинката

### Вход
```
3840 2160 2207 2005
```
### Изход
```
7697567
```