# Keylogger

Инсталирах **keylogger** на училищния компютър - програма, която следи и записва всяко натискане на клавиш. С нейна помощ исках да вляза в електронния дневник на училището и да променя оценките. Тя изисква всеки учител да напише някакъв PIN код, съставен от цифрите **1 2 3 4 5 6 7 8 9** (забележете, че няма **0**).   
  
Е, всичко вървеше по план, докато не стигнах следния проблем - хората често бъркат и се поправят, докато пишат. В записите на keylogger<sup>-а</sup> имам цифрите, които учителите пишат, както и цифрата **0**, когато натискат backspace (бутона за изтриване).  

Например, поредицата **1 0 3 4 4 0 0 5 5** означава, че някой е натиснал бутона 1, после backspace (изтриване на един символ), после бутона 3, бутона 4, пак бутона 4, после два backspace<sup>-а</sup> и накрая два пъти бутона 5. В резултат PIN кодът е `"3 5 5"`.

Напишете програма, която по даден запис от keylogger<sup>-а</sup>, възстановява какъв е истинският PIN код.

# Формат вход/изход
На първия ред от входа има едно число, показващо колко натискания на клавиши е хванал keylogger<sup>-ът</sup>.  

На следващия ред следват толкова цифри - всички хванати клавиши, където **0** означава backspace.

# Ограничения
Всички записи на keylogger<sup>-а</sup> съдържат до `1000` натискания.  
Ако е натиснат backspace, то със сигурност има цифра, която може да бъде изтрита.  
**В 30% oт тестоветe няма повече от едно поредно натискане на backspace.**

# Пример

### Вход
```
3
1 2 3
```
### Изход
```
1 2 3
```
### Пояснение
Няма изтриване, PIN кодът може да се изведе директно.

### Вход
```
30
9 0 4 5 2 4 1 9 3 0 3 3 0 6 0 3 0 4 0 0 0 7 4 0 4 3 3 6 6 0
```
### Изход
```
4 5 2 4 1 7 4 3 3 6
```