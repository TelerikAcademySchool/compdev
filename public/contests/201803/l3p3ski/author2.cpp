#include <iostream>
using namespace std;

int n, l;
int d[100200], k[100200];

int main(){
    cin>>n>>l;
    for (int i=0; i<n; ++i){
        cin>>d[i];
    }
    for (int i=0; i<n; ++i){
        cin>>k[i];
    }
    int ans=0, curr=0;
    int to=0;
    for (int from=0; from<n; ++from){
        to = max(from, to);
        while (to<n && d[to]-d[from]<=l){
            curr+=k[to];
            ++to;
        }
        if (curr>ans){ans=curr;}
        curr-=k[from];
    }
    cout<<ans<<'\n';
    return 0;
}
