#include <iostream>
#include <random>
#include <stdlib.h>

using namespace std;

const int MAX = 1000000;

int main() 
{
	random_device rd;
	default_random_engine eng(rd());
	uniform_int_distribution<int> dist(-MAX, MAX);
    
	cout << dist(eng) << ' ' << dist(eng) << ' ' << dist(eng) << '\n';

    return 0;
}
