#include <iostream>

using namespace std;

int main ()
{
	int a, b, c;
	cin >> a >> b >> c;

	int minn = a;
	if (b < minn)
	{
		minn = b;
	}
	if (c < minn)
	{
		minn = c;
	}

	int maxx = a;
	if (b > maxx)
	{
		maxx = b;
	}
	if (c > maxx)
	{
		maxx = c;
	}

	cout << minn + maxx << '\n';

	return 0;
}
