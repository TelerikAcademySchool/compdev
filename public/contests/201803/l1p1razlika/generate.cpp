#include <iostream>
#include <cstdio>
#include <stdlib.h>
using namespace std;

void test(const char* filename, int a, int b){
    freopen(filename, "w", stdout);
    cout<<a<<" "<<b<<"\n";
    fclose(stdout);
}

int main(){
    srand(42);
    test("test.000.001.in.txt", 3, 5);
    test("test.000.002.in.txt", 10, 1);
    test("test.001.in.txt", rand()%10+1, rand()%10+1);
    test("test.002.in.txt", rand()%100+1, rand()%100+1);
    test("test.003.in.txt", rand()%100+1, rand()%100+1);
    test("test.004.in.txt", rand()%100+1, rand()%100+1);
    test("test.005.in.txt", rand()%100+1, rand()%100+1);
    test("test.006.in.txt", rand()%1000+1, rand()%1000+1);
    test("test.007.in.txt", rand()%1000+1, rand()%1000+1);
    test("test.008.in.txt", 1742, 1742);
    test("test.009.in.txt", rand()%1000+1, rand()%1000+1);
    test("test.010.in.txt", rand()%1000+1, rand()%1000+1);
    return 0;
}
