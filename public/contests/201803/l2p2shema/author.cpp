#include <iostream>
using namespace std;

int main() {
	int x, y;
	char simvol;

	cin >> x >> y >> simvol;
	if(y > x) {
		int tmp = x;
		x = y;
		y = tmp;
	}

	for(int red = 0;red < y;red = red+1) {
		for(int kolona = 0;kolona < x;kolona = kolona + 1) {
			cout << simvol;
		}
		cout << '\n';
	}
    return 0;
}

