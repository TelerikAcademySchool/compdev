#include <iostream>
using namespace std;
int main(){
    int n, k;
    char word[64];
    cin>>n>>k;
    cin>>word;
    int ans = 1;
    for (int i=0; i<n; ++i){
        char minw = word[i], maxw = word[i];
        for (int j=i+1; j<n; ++j){
            if (word[j]>maxw) maxw = word[j];
            if (word[j]<minw) minw = word[j];
            if (maxw - minw >= k){break;}
            if (ans < j-i+1){ans = j-i+1;}
        }
    }
    cout<<ans<<"\n";
    return 0;
}
