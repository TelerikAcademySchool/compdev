#include<iostream>
using namespace std;
int main()
{
    int n,k, otg = 1;
    string s;
    cin>>n>>k>>s;
    for(int i=0;i<n;i++)
    {
        for(int j=i+1;j<n;j++)
        {
            bool fail = false;
            for(int a=i;a<=j;a++)
            {
                for(int b=i;b<=j;b++)
                {
                    int diff = s[a]-s[b];
                    if(diff>=k || diff*(-1)>=k)
                    {
                        fail = true;
                    }
                }
            }
            if(!fail)
            {
                otg = max(otg, j-i+1);
            }
        }
    }
    cout<<otg<<endl;
    return 0;
}
