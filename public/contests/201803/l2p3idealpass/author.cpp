#include <iostream>

using namespace std;

int main ()
{
	int n, m;
	cin >> n >> m;

	char lastLetter = 'a' + m - 1;

	int count = m * n * (m - 1) * (9 - n + 1) * (n - 1);
	cout << count << '\n';

	if (count > 0)
	{
		int found = 0;
		for (char a = 'a'; a <= lastLetter; ++a)
		{
			for (int b = 1; b <= n; ++b)
			{
				for (char c = 'a'; c <= lastLetter; ++c)
				{
					if (c != a)
					{
						for (int d = n; d <= 9; ++d)
						{
							for (int e = 1; e <= n; ++e)
							{
								if (e != b)
								{
									cout << a << b << c << d << e;
									if (found + 1 < count)
									{
										cout << ' ';
									}
									++found;
								}
							}
						}
					}
				}
			}
		}
		cout << '\n';
	}
	return 0;
}
