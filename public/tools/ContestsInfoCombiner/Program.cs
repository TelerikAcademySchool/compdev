﻿using System;
using System.IO;
using System.Collections.Generic;

namespace ContestsInfoCombiner
{
    class Program
    {
        const int GetForOneTask = 1;
        const int GetForManyTasks = 2;

        static void Main(string[] args)
        {
            int choice = GetForManyTasks;

            if(choice == GetForOneTask){
                string path = @"C:\Users\eliza\OneDrive - Telerik Academy\Competitive Development\Tasks\gitlab\telerik-academy-judge-tasks\School\topic1720workshop\p05spirits";
                GetTests(path);
            } else if(choice == GetForManyTasks){
                string contestsPath = @"C:\Users\eliza\OneDrive - Telerik Academy\Competitive Development\Tasks\Competitive Development 4-12 repo\compdev\public\exercisetasks";//Console.ReadLine();

                string[] contestDirs = Directory.GetDirectories(contestsPath);

                foreach(string contest in contestDirs)
                {
                    if(contest.Contains("topic"))
                    {
                        string[] tasksPaths = Directory.GetDirectories(contest);
                        foreach(string task in tasksPaths)
                        {
                            GetTests(task);
                            //Console.WriteLine(task);
                        }
                    }
                }
            }
        }

        static void GetTests(string taskPath){
            string[] fileEntries = Directory.GetFiles(taskPath);
            List<string> allTests = new List<string>();
            int counter = 1;
            string input = "";
            string output = "";

            allTests.Add("№ | Вход | Изход");
            allTests.Add("--- | --- | --- ");

            foreach(string path in fileEntries) 
            {
                string fileName = Path.GetFileName(path);
                //Console.WriteLine("Processed file '{0}'.", fileName);
                if(fileName.Contains("test"))
                {
                    if(fileName.Contains("in")) {
                        input = System.IO.File.ReadAllText(path);
                        input = input.Trim();
                        input = input.Replace(System.Environment.NewLine, " <br/> ");
                    }

                    if(fileName.Contains("out")) {
                        output = System.IO.File.ReadAllText(path);
                        output = output.Trim();
                        output = output.Replace(System.Environment.NewLine, " <br/> ");
                        string newTest = counter + " | " + input + " | " + output;
                        counter++;
                        allTests.Add(newTest);
                        //Console.WriteLine(newTest);
                    }
                    
                }
            }
            
            File.WriteAllLines(taskPath + "\\ALL_TESTS.md", allTests); 
            Console.WriteLine("Ready with " + taskPath);  
        }
    }
}
