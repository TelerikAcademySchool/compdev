# include <iostream>
using namespace std;

int main(){
    int d,m,g,last;
    cin>>d>>m>>g;
    if (m==2){
        if ((g%4==0 && g%100!=0 )|| g%400==0){
            last=29;
        } else {
            last=28;
        }
    } else {
        if (m==4 || m==6 || m==9 || m==11){
            last=30;
        } else {
            last=31;
        }
    }

    if (d<last){
        d++;
    } else {
        d=1;
        if (m==12){
            m=1;  g++;
        } else {
            m++;
        }
    }

    cout<<d<<" "<<m<<" "<< g<<endl;
    return 0;
}
