# include <iostream>
using namespace std;

int main(){
    int d,m,g;
    cin>>d>>m>>g;

    if (d>1){
        d--;
    } else {
        if (m==3){
            if ((g%4==0 && g%100!=0 )|| g%400==0){
                d=29;
            } else {
                d=28;
            }
        } else {
            if (m==1 || m==2 || m==4 || m==6 || m==8 || m==9 || m==11){
                d=31;
            } else {
                d=30;
            }
        }

        if (m==1){
            m=12;
            g--;
        } else {
            m--;
        }
    }

    cout<<d<<" "<<m<<" "<< g<<endl;
    return 0;
}
