# include <iostream>
using namespace std;

int main(){
    int d,m,g,c;
    cin>>d>>m>>g>>c;

    d+=c-1;
    if (m==2){
        if ((g%4==0 && g%100!=0 )|| g%400==0){
            if(d>29){
                d = d - 29;
                m++;
            }
        } else {
            if(d>28){
                d = d - 28;
                m++;
            }
        }
    } else {
        if (m==4 || m==6 || m==9 || m==11){
            if(d>30){
                d = d - 30;
                m++;
            }
        } else {
            if(d>31){
                d = d - 31;
                m++;

                if(m==13){
                    m = 1;
                    g++;
                }
            }
        }
    }

    cout<<d<<" "<<m<<" "<< g<<endl;
    return 0;
}
