#include <iostream>
using namespace std;

int main(){
    int n;
    cin>>n;

    if(n>=1&&n<=7){
        n+=3;
        if(n>7){
            n-=7;
        }

        switch(n)
        {
            case 1:cout<<"ponedelnik";break;
            case 2:cout<<"vtornik";break;
            case 3:cout<<"sriada";break;
            case 4:cout<<"chetvurtuk";break;
            case 5:cout<<"petak";break;
            case 6:cout<<"subota";break;
            case 7:cout<<"nedelia";break;
        }
    }else{
        cout<<"nevaliden den";
    }

    return 0;
}
