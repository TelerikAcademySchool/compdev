#include <iostream>
using namespace std;

int main()
{
    const double PI = 3.14;
    double r, p, s;
    cin>>r;
    p = 2*PI*r;
    s = PI*r*r;
    cout<<p<<" "<<s<<endl;
    return 0;
}
