Напишете програма, която определя дали дадено число е четно или нечетно.

## Вход
На входа се въвежда едно число.

## Изход
Извежда се:
* ``chetno``, ако числото е четно.  
* ``nechetno``, ако числото е нечетно.  

## Ограничения
* ``0`` <= ``число`` <= ``1 000 000 000 000``

## Пример

### Вход:
```
25
```
### Изход: 
```
nechetno
```
  
### Вход:
```
234356
```
### Изход: 
```
chetno
```