Индексът на телесната маса (BMI) се изчислява като разделим килограмите на даден човек на квадрата от височината му в метри. Например човек, който тежи 50 килограма и е висок 164см. има BMI=18.6 (височината в метри е 1.64м и на квадрат е 1.64*1.64=2,6896, като разделим килограмите на височината на квадрат се получава приблизително 50/2,6896=18.6). Според този индекс хората се делят на няколко категории. Напишете програма, която при въведени килограми и височина в сантиметри, изчислява индекса на телесната маса и извежда съвет за дадения човек в зависимост от категорията, в която попада.


## Вход
На първия ред се въвеждат килограмите на човека, а на втория височината в сантиметри.

## Изход
Извежда се:
* ``biagai za neshto mazno``, ако индекса на телесната маса е по-малък от ``18.5``.  
* ``bravo be, levent``, ако индекса на телесната маса e равен или по-голям на ``18.5`` и по-малък от ``24.9``.  
* ``stiga s banichkite``, ако индекса на телесната маса e равен или по-голям на ``24.9`` и по-малък от ``29.9``. 
* ``spirai sladkoto!``, ако индекса на телесната маса е равен или по-голям от ``29.9``.  

## Ограничения
* ``1`` <= ``тегло`` <= ``650``
* ``1`` <= ``височина`` <= ``300``

## Пример

### Вход:
```
50
164
```
### Изход: 
```
bravo be, levent
```
  
### Вход:
```
100
174.5
```
### Изход: 
```
spirai sladkoto!
```