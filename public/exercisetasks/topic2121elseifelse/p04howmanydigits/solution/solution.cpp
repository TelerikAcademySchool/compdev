#include<iostream>
using namespace std;

int main()
{
    long long n;
    cin>>n;

    if (n<10){
         cout << 1;
    } else if(n<100){
        cout << 2;
    } else if(n<1000){
        cout << 3;
    } else if(n<10000){
        cout << 4;
    } else if(n<100000){
        cout << 5;
    } else if(n<1000000){
        cout << 6;
    } else if(n<10000000){
        cout << 7;
    } else if(n<100000000){
        cout << 8;
    } else if(n<1000000000){
        cout << 9;
    } else if(n<10000000000){
        cout << 10;
    } else if(n<100000000000){
        cout << 11;
    }

    return 0;
}
