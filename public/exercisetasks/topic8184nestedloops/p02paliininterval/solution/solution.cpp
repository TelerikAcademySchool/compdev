#include<iostream>

using namespace std;

int main() {
  int m, n;
  int num = 0;
  cin >> m >> n;

  for (int i = m; i <= n; i++) {
    int help = i;

    while (help != 0) {
      num = 10 * num + help % 10;
      help = help / 10;
    }

    if (num == i) {
      cout << i << "\n";
    }

    num = 0;
  }

  return 0;
}
