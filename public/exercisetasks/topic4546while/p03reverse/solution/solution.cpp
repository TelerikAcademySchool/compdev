#include <iostream>
using namespace std;
int main()
{
    long long num;
    long long revnum = 0;
    int temp;
    cin>>num;

    while (num>0) {
        temp=num%10;
        revnum=revnum*10+temp;
        num=num/10;
    }

    cout<<revnum<<endl;

    return 0;
}
