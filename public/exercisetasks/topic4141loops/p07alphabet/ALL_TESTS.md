№ | Вход | Изход
--- | --- | --- 
1 | q | q r s t u v w x y z
2 | W | W X Y Z
3 | a | a b c d e f g h i j k l m n o p q r s t u v w x y z
4 | A | A B C D E F G H I J K L M N O P Q R S T U V W X Y Z
5 | f | f g h i j k l m n o p q r s t u v w x y z
6 | F | F G H I J K L M N O P Q R S T U V W X Y Z
7 | k | k l m n o p q r s t u v w x y z
8 | K | K L M N O P Q R S T U V W X Y Z
9 | r | r s t u v w x y z
10 | Z | Z
