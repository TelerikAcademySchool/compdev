№ | Вход | Изход
--- | --- | --- 
1 | 1000 <br/> 95 <br/> 70 | 357.143
2 | 100 <br/> 70 <br/> 40 | 75
3 | 500 <br/> 90 <br/> 50 | 400
4 | 750 <br/> 40 <br/> 15 | 1250
5 | 350 <br/> 70 <br/> 8 | 2712.5
6 | 700 <br/> 40 <br/> 70 | Jelanata koncentracia e po-goliama ot tekushtata!
7 | 150 <br/> 70 <br/> 70 | 0
