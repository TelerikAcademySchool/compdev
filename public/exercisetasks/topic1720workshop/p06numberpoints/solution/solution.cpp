#include <iostream>
using namespace std;

int main() {
	int n, edinici, desetici, stotici, sum, product;
	int score = 0;
	cin >> n;

	edinici = n % 10;
	desetici = (n%100) / 10;
	stotici = n / 100;

	product = edinici * desetici * stotici;
	sum = edinici + desetici + stotici;

	if (n%2 == 0) {
        score++;
	}

    if (n%5 == 0) {
        score++;
	}

    if (product > sum) {
        score++;
	}

	if (n%3 == 2) {
        score++;
	}

	if ((n-13)%2 == 1) {
        score++;
	}

	if (edinici%2==1 && desetici%2==1 && stotici%2==1) {
        score++;
	}

	cout << score;

	return 0;
}
