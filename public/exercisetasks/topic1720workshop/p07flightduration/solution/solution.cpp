#include<iostream>
using namespace std;

int main()
{
    int a1, a2, b1, b2;
    cin >> a1 >> a2;
    cin >> b1 >> b2;

    int a = a1*60 + a2;
    int b = b1*60 + b2;

    if(a >= b){
        b = b + 1440;
    }

    int c = b - a;
    cout << c/60 << " " << c%60;

    return 0;
}
