#include <iostream>

using namespace std;

int main() {
    const float SQF = 10.7639104;
    const float SQI = 1550.0031;
    const float SQY = 1.19599005;
	float sqm;
	int choice;
	cin >> sqm >> choice;

	if(choice == 1){
        cout << sqm * SQF;
	}

	if(choice == 2){
        cout << sqm * SQI;
	}

	if(choice == 3){
        cout << sqm * SQY;
	}

	return 0;
}
