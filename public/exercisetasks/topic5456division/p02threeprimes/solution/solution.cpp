#include <iostream>
using namespace std;

int main(){
    int n,m,k;
    cin>>n>>m>>k;
    bool prostoN=true;
    bool prostoM=true;
    bool prostoK=true;

    for (int i=2; i<=n/2; i++){
        if (n%i==0)  {
            prostoN=false;
            break;
        }
    }

    for (int i=2; i<=m/2; i++){
        if (m%i==0)  {
            prostoM=false;
            break;
        }
    }

    for (int i=2; i<=k/2; i++){
        if (k%i==0)  {
            prostoK=false;
            break;
        }
    }

    if (prostoN){
        cout<<"da"<<" ";
    } else {
        cout<<"ne"<<" ";
    }

    if (prostoM){
        cout<<"da"<<" ";
    } else {
        cout<<"ne"<<" ";
    }

    if (prostoK){
        cout<<"da"<<endl;
    } else {
        cout<<"ne"<<endl;
    }

    if(prostoN&&prostoM&&prostoK){
        cout<<"URA"<<endl;
    }

    return 0;
}
