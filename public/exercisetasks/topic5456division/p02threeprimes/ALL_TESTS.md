№ | Вход | Изход
--- | --- | --- 
1 | 3 8 100 | da ne ne
2 | 17 97 23 | da da da <br/> URA
3 | 111 17 23 | ne da da
4 | 12321 324 1123 | ne ne da
5 | 22 8934 23132 | ne ne ne
6 | 1000000 1000000 1000000 | ne ne ne
7 | 2 2 2 | da da da <br/> URA
8 | 7 17 222 | da da ne
9 | 23348 7 23 | ne da da
10 | 7 89 3 | da da da <br/> URA
