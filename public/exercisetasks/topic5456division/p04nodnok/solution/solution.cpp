#include <iostream>
using namespace std;

int main(){
    long long n, m;
    long long orgn, orgm;
    cin>>n>>m;
    orgn = n;
    orgm = m;

    while (m!=n){
        if (n>m) {
            n=n-m;
        } else {
            m=m-n;
        }
    }

    cout<<m<<" "<<(orgn*orgm)/m;
    return 0;
}
