#include <iostream>
using namespace std;

int main(){
    int n, m, dign, digm, temp;
    int sum = 0;
    cin>>n>>m;
    dign = n;
    digm = m;

    do
    {
        temp=dign%10;
        sum+=temp;
        dign=dign/10;
    }
    while(dign!=0);

    do
    {
        temp=digm%10;
        sum+=temp;
        digm=digm/10;
    }
    while(digm!=0);

    //cout<<sum;

    while (m!=n){
        if (n>m) {
            n=n-m;
        } else {
            m=m-n;
        }
    }

    //cout<<"\n"<<m<<"\n";

    cout<<m%sum;

    return 0;
}
