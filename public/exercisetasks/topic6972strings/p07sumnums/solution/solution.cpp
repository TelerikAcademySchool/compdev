#include <iostream>
#include <cstring>
using namespace std;

int main(){
    string userinput, text;
    getline(cin,userinput);
    int sum=0;
    int number=0;
    text = userinput + " "; //tova e, za da moje da hvashtame chisla, koito sa posledni v niza

    for (int i=0; i<text.size(); i++){
        if ((int) text[i]>=48 && (int) text[i]<= 57 ){
            number=10*number + (text[i]-'0');
        } else {
            sum+=number;
            number=0;
        }
    }

    cout<<sum<<endl;

    return 0;
}
