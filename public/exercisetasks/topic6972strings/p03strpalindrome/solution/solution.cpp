#include<iostream>
#include <cstring>
using namespace std;

int main(){
    string text;
    getline(cin, text);
    bool isPalindrome = true;
    int n = text.size();

    for (int i=0; i<n/2; i++){
        if(text[i]!=text[n-i-1]){
            isPalindrome = false;
            break;
        }
    }

    if(isPalindrome){
        cout<<"da";
    }else{
        cout<<"ne";
    }

    return 0;
}
