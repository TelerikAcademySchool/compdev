Напишете програма, в която се въвежда изречение и се извежда броя на еднобуквените думи в него.

## Вход
На един ред се въвежда изречението.  

## Изход
Извежда се броя на еднобуквените думи.

## Ограничения
* ``10`` <= ``дължина на изречението`` <= ``100``
* Изречението може да съдържа малки и главни латински букви и интервали.

## Пример

### Вход: 
```
Neka otidem na razhodka v gorata i da si napravim piknik
```
### Изход: 
```
2
```