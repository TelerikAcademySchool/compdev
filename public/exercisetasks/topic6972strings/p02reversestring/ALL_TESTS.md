№ | Вход | Изход
--- | --- | --- 
1 | Kak ste, dragi priateli? | ?iletairp igard ,ets kaK
2 | Yoh e spasil Anna ot strashnata i sila! | !alis i atanhsarts to annA lisaps e hoY
3 | Hao i Yoh sa bratia bliznaci! Shok i ujas za vsichki! | !ikhcisv az saju i kohS !icanzilb aitarb as hoY i oaH
4 | Kral na shamanite shte stane Hao - interesno avtorsko reshenie. | .einehser oksrotva onseretni - oaH enats eths etinamahs an larK
5 | a | a
6 | eho | ohe
7 | ne | en
8 | Naistina interesno reshenie loshia geroi da vzeme titlata, makar i da ;w se poraztapia sarceto malko | oklam otecras aipatzarop es w; ad i rakam ,ataltit emezv ad ioreg aihsol einehser onseretni anitsiaN
9 | I produljavame s filosofiite za testovi danni. | .innad ivotset az etiifosolif s emavajludorp I
10 | Ako niakoi niakoga chete tova - have fun. Vduhnovenieto e ot animeto Shaman king - preporuchvam ;d | d; mavhcuroperp - gnik namahS otemina to e oteinevonhudV .nuf evah - avot etehc agokain iokain okA
