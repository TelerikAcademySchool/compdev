№ | Вход | Изход
--- | --- | --- 
1 | aasdfgaas | 9 <br/> a f s
2 | 12345 | 5 <br/> 1 3 5
3 | qwert | 5 <br/> q e t
4 | zxc | 3 <br/> z x c
5 | f | 1 <br/> f f f
6 | jkllkjjwaljkllkjjwaljkllkjjwaljkllkjjwaljkllkjjwaljkllkjjwaljkllkjjwaljkllkjjwaljkllkjjwaljkllkjjwal | 100 <br/> j j l
7 | rst | 3 <br/> r s t
8 | asdfgasdfga | 11 <br/> a a a
9 | 567 | 3 <br/> 5 6 7
10 | dskjfddskflkdjfkldmWnfkdkfjdskjfkdslnlkmdnflkdfdkfrijWKIJLFMLFFJFNKNFKJDHKJK | 76 <br/> d k K
