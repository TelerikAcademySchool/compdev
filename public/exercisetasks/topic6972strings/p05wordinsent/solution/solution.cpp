#include<iostream>
#include <cstring>
using namespace std;

int main(){
    string text, word;
    getline(cin, text);
    getline(cin, word);
    int pos = text.find(word);
    int br = 0;

    while (pos!=-1)
    {
        br++;
        pos = text.find(word, pos+1);
    }

    cout<<br;

    return 0;
}
