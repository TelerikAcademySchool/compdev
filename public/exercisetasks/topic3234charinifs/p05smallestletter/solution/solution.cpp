# include <iostream>
using namespace std;

int main(){
    char ch1, ch2, ch3;
    int code1, code2, code3, mincode;

    cin>>ch1>>ch2>>ch3;
    code1 = ch1;
    code2 = ch2;
    code3 = ch3;
    mincode = code1;
    if(code2 < mincode){
        mincode = code2;
    }

    if(code3 < mincode) {
        mincode = code3;
    }

    cout<<(char)mincode;

    return 0;
}
