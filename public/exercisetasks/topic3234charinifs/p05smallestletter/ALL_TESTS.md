№ | Вход | Изход
--- | --- | --- 
1 | k c x | c
2 | a n v | a
3 | m a c | a
4 | m v z | m
5 | d j z | d
6 | x y z | x
7 | c j k | c
8 | c k b | b
9 | z d k | d
10 | u w r | r
