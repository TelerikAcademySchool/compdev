№ | Вход | Изход
--- | --- | --- 
1 | N C X F | X
2 | Z A H D | Z
3 | M A C S | S
4 | M V B G | V
5 | D J Z X | Z
6 | M I A N | N
7 | L O C A | O
8 | F O A D | O
9 | A B C D | D
10 | Z Y X W | Z
