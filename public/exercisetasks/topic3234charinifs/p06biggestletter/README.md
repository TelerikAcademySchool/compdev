Създайте програма, в която се въвеждат четири главни латински букви и се извежда буквата, която се намира най-назад в реда на азбуката.

## Вход
На входа се въвеждат четири главни латински букви, отделени с интервал.

## Изход
Извежда се буквата, която се намира най-назад в реда на азбуката.

## Ограничения
* ``a`` <= ``буква`` <= ``z``
* гарантирано е, че буквите винаги ще са различни

## Пример

### Вход:
```
N C X F
```
### Изход: 
```
X
```