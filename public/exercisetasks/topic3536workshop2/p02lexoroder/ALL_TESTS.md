№ | Вход | Изход
--- | --- | --- 
1 | nZb | 1.Z <br/> 2.b <br/> 3.n
2 | ANZ | 1.A <br/> 2.N <br/> 3.Z
3 | MCQ | 1.C <br/> 2.M <br/> 3.Q
4 | ZWL | 1.L <br/> 2.W <br/> 3.Z
5 | abc | 1.a <br/> 2.b <br/> 3.c
6 | vbn | 1.b <br/> 2.n <br/> 3.v
7 | xyj | 1.j <br/> 2.x <br/> 3.y
8 | aAn | 1.A <br/> 2.a <br/> 3.n
9 | kXe | 1.X <br/> 2.e <br/> 3.k
10 | FjG | 1.F <br/> 2.G <br/> 3.j
11 | lNM | 1.M <br/> 2.N <br/> 3.l
12 | AbC | 1.A <br/> 2.C <br/> 3.b
13 | zZa | 1.Z <br/> 2.a <br/> 3.z
14 | ouH | 1.H <br/> 2.o <br/> 3.u
15 | hwK | 1.K <br/> 2.h <br/> 3.w
