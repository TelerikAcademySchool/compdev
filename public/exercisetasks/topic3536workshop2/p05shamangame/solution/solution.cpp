# include <iostream>
using namespace std;

int main(){
    char ch1, ch2, ch3;
    int num1, num2, num3;
    int fin1, fin2, fin3;
    int edinici1, desetici1, stotici1, edinici2, desetici2, stotici2, edinici3, desetici3, stotici3;

    cin>>ch1>>ch2>>ch3;
    num1 = ch1;
    num2 = ch2;
    num3 = ch3;
    edinici1 = num1%10;
    desetici1 = (num1%100)/10;
    //Ако числото е двуцифрено, тук ще се получи 0 , което не е проблем, защото
    //сами при главните букви умножаваме, а те винаги са с двуцифрени кодове
    stotici1 = num1/100;

    edinici2 = num2%10;
    desetici2 = (num2%100)/10;
    stotici2 = num2/100;

    edinici3 = num3%10;
    desetici3 = (num3%100)/10;
    stotici3 = num3/100;

    if (num1>=65 && num1<=90){
        fin1 = edinici1*desetici1;
    } else {
        fin1 = edinici1+desetici1+stotici1;
    }

    if (num2>=65 && num2<=90){
        fin2 = edinici2*desetici2;
    } else {
        fin2 = edinici2+desetici2+stotici2;
    }

    if (num3>=65 && num3<=90){
        fin3 = edinici3*desetici3;
    } else {
        fin3 = edinici3+desetici3+stotici3;
    }

    int maxnum = fin1;

    if(maxnum<fin2){
        maxnum = fin2;
    }

    if(maxnum<fin3){
        maxnum = fin3;
    }

    cout<<maxnum<<endl;

    return 0;
}
