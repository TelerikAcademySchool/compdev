№ | Вход | Изход
--- | --- | --- 
1 | f <br/> X <br/> c | 64
2 | A <br/> n <br/> Z | 30
3 | w <br/> P <br/> b | 17
4 | Z <br/> W <br/> L | 56
5 | a <br/> b <br/> c | 18
6 | v <br/> b <br/> n | 17
7 | x <br/> y <br/> j | 7
8 | a <br/> A <br/> n | 30
9 | k <br/> X <br/> e | 64
10 | F <br/> j <br/> G | 7
11 | l <br/> N <br/> M | 56
12 | A <br/> b <br/> C | 42
13 | z <br/> Z <br/> a | 16
14 | o <br/> u <br/> H | 14
15 | h <br/> w <br/> K | 35
