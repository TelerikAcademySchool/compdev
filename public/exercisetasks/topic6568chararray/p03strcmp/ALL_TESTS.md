№ | Вход | Изход
--- | --- | --- 
1 | abcde <br/> klmno | abcde
2 | mnp <br/> bcd | bcd
3 | hao <br/> hao | ravni
4 | hapvane  <br/> piivane | hapvane
5 | ednozamenednozatebi <br/> fjdlfdjlflsjfdkgjkf | ednozamenednozatebi
6 | nintirimintiri <br/> adsdlksfjlkdsf | adsdlksfjlkdsf
7 | a <br/> n | a
8 | da <br/> as | as
9 | tuk <br/> tuk | ravni
10 | zapetaia <br/> duma | duma
