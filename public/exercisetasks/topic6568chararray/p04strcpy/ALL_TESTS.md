№ | Вход | Изход
--- | --- | --- 
1 | dorodoro | torotoro <br/> dorodoro
2 | bcd | bct <br/> bcd
3 | hao | hao <br/> hao
4 | dulalula | tulalula <br/> dulalula
5 | ednozamenednozatebi | etnozamenetnozatebi <br/> ednozamenednozatebi
6 | dadadadadadadadadadadadadadad | tatatatatatatatatatatatatatat <br/> dadadadadadadadadadadadadadad
7 | d | t <br/> d
8 | da | ta <br/> da
9 | darmad | tarmat <br/> darmad
10 | duma | tuma <br/> duma
