№ | Вход | Изход
--- | --- | --- 
1 | 3 <br/> kak <br/> ste <br/> priateli | kak ste priateli <br/> kak ste priateli
2 | 1 <br/> zdravei | zdravei <br/> zdravei
3 | 2 <br/> moi <br/> preateli | moi preateli <br/> moi preateli
4 | 4 <br/> magareta <br/> kone <br/> ponita <br/> neshto | magareta kone ponita neshto <br/> magareta kone ponita neshto
5 | 5 <br/> hao <br/> niama <br/> priateli <br/> vse <br/> oshte | hao niama priateli vse oshte <br/> hao niama priateli vse oshte
6 | 4 <br/> no <br/> shte <br/> si <br/> nameri | no shte si nameri <br/> no shte si nameri
7 | 4 <br/> i <br/> shte <br/> stane <br/> dobur | i shte stane dobur <br/> i shte stane dobur
8 | 5 <br/> a <br/> b <br/> c <br/> d <br/> e | a b c d e <br/> a b c d e
9 | 2 <br/> da <br/> ne | da ne <br/> da ne
10 | 5 <br/> dulgaduma <br/> dulgaduma <br/> dulgaduma <br/> dulgaduma <br/> dulgaduma | dulgaduma dulgaduma dulgaduma dulgaduma dulgaduma <br/> dulgaduma dulgaduma dulgaduma dulgaduma dulgaduma
