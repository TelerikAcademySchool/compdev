Направете програма, която въвежда от клавиатурата числото ``n`` и след това ``n`` на брой символни низа като програмата трябва да ги долепи един до друг, разделяйки ги с интервал, и да изведе получилия се голям низ два пъти на отделни редове. 

## Вход
На първия ред се въвежда числото ``n``.  
На следващите редове се въвеждат ``n`` на брой низа, всеки на отделен ред.

## Изход
Извежда се два пъти низът, образуван от долепянето на всички въведени низове, разделени с интервал.

## Ограничения
* ``1`` <= ``n`` <= ``5``
* ``1`` <= ``дължина на низ`` < ``10``
* гарантирано е, че низовете няма да съдържат празни разстояния (спейсове)

## Пример

### Вход: 
```
3
kak
ste
priateli
```
### Изход: 
```
kak ste priateli
kak ste priateli
```
