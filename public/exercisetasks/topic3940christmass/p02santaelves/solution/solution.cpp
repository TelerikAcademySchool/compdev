# include <iostream>
using namespace std;

int main(){
    int d, m, y;
    char code, rev;
    cin>>d>>m>>y>>code>>rev;

    if(code=='.' || code=='/' || code=='\\' || code=='-'){
        if(rev=='>'){
            if(d<10){
                cout<<'0';
            }

            cout<<d<<code;
            if(m<10){
                cout<<'0';
            }

            cout<<m<<code<<y<<endl;
        } else if(rev=='<'){
            cout<<y<<code;
            if(m<10){
                cout<<'0';
            }

            cout<<m<<code;
            if(d<10){
                cout<<'0';
            }

            cout<<d<<endl;
        }
    } else {
        cout<<"Tozi format ne vaji!";
    }

    return 0;
}
