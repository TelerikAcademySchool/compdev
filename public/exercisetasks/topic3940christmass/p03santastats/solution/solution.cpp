# include <iostream>
using namespace std;

int main(){
    int d1,m1,g1,d2,m2,g2,n,mdays;
    cin>>d1>>m1>>g1>>d2>>m2>>g2;

    if(g1==g2){
        if(m1==m2){
            n = d2-d1+1;
        } else {
            if (m1==2){
                if ((g1%4==0 && g1%100!=0 )|| g1%400==0){
                    mdays=29;
                } else {
                    mdays=28;
                }
            } else {
                if (m1==4 || m1==6 || m1==9 || m1==11){
                    mdays=30;
                } else {
                    mdays=31;
                }
            }

            n = (mdays-d1) + d2 + 1;
        }
    } else {
        //ako godinite sa razlichni znahci, che nachalnaha data e dekemvri, a krainata ianuari
        n = (31-d1) + d2 + 1;
    }

    cout<<n<<endl;
    return 0;
}
