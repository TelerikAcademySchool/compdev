# include <iostream>
using namespace std;

int main(){
    int d,m,g;
    int dd,de,md,me,gh,gs,gd,ge;
    cin>>d>>m>>g;

    de = d%10;
    dd = d/10;
    me = m%10;
    md = m/10;
    ge = g%10;
    gd = (g%100)/10;
    gs = (g%1000)/100;
    gh = g/1000;

    cout<<d<<" ";
    switch(m) {
      case 1: cout<<"January "; break;
      case 2: cout<<"February "; break;
      case 3: cout<<"March "; break;
      case 4: cout<<"April "; break;
      case 5: cout<<"May "; break;
      case 6: cout<<"June "; break;
      case 7: cout<<"July "; break;
      case 8: cout<<"August "; break;
      case 9: cout<<"September "; break;
      case 10: cout<<"October "; break;
      case 11: cout<<"November "; break;
      case 12: cout<<"December "; break;
    }

    cout<<g<<endl;
    cout<<de+dd+me+md+ge+gd+gs+gh<<" gifts";
    return 0;
}
