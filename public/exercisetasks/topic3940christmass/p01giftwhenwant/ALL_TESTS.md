№ | Вход | Изход
--- | --- | --- 
1 | 12 3 2020 | 12 March 2020 <br/> 10 gifts
2 | 2 12 3024 | 2 December 3024 <br/> 14 gifts
3 | 25 9 5341 | 25 September 5341 <br/> 29 gifts
4 | 1 1 2900 | 1 January 2900 <br/> 13 gifts
5 | 31 1 9500 | 31 January 9500 <br/> 19 gifts
6 | 28 2 2300 | 28 February 2300 <br/> 17 gifts
7 | 31 3 2034 | 31 March 2034 <br/> 16 gifts
8 | 30 4 2450 | 30 April 2450 <br/> 18 gifts
9 | 31 5 2993 | 31 May 2993 <br/> 32 gifts
10 | 30 6 2098 | 30 June 2098 <br/> 28 gifts
11 | 31 7 7923 | 31 July 7923 <br/> 32 gifts
12 | 31 8 2028 | 31 August 2028 <br/> 24 gifts
13 | 30 9 2222 | 30 September 2222 <br/> 20 gifts
14 | 31 10 2456 | 31 October 2456 <br/> 22 gifts
15 | 30 11 2500 | 30 November 2500 <br/> 12 gifts
16 | 31 12 2499 | 31 December 2499 <br/> 31 gifts
