№ | Вход | Изход
--- | --- | --- 
1 | 16 <br/> 13 5 14 26 5 12 26 9 0 1 4 0 5 4 9 1 | aide da izleznem
2 | 17 <br/> 13 1 22 19 5 18 1 8 0 5 20 0 15 7 15 14 13 | mnogo te haresvam
3 | 29 <br/> 20 8 19 15 14 0 1 11 5 12 0 5 14 1 7 1 9 12 0 1 26 0 5 0 5 13 5 18 22 | vreme e za liagane leka nosht
4 | 1 <br/> 1 | a
5 | 42 <br/> 9 20 0 1 0 13 1 7 1 9 12 0 9 19 0 1 4 0 1 9 12 19 9 13 0 1 16 19 15 4 0 5 19 0 9 13 0 15 7 15 14 13 | mnogo mi se dospa mislia da si liagam a ti
6 | 6 <br/> 9 19 0 11 1 11 | kak si
7 | 75 <br/> 1 14 7 15 13 15 16 0 9 20 0 1 4 0 1 7 15 13 0 15 14 19 5 18 5 20 14 9 0 15 7 15 14 13 0 5 8 19 5 2 0 1 9 18 15 20 19 9 0 15 16 0 15 20 15 14 8 19 1 13 15 4 0 9 19 0 9 12 0 1 19 9 16 1 14 | napisa li si domashnoto po istoria beshe mnogo interesno moga da ti pomogna
8 | 95 <br/> 9 12 0 8 19 1 11 19 9 0 11 5 22 15 8 3 0 14 9 4 5 0 5 20 8 19 15 0 1 26 0 15 20 19 1 9 13 0 1 13 9 0 1 20 1 12 15 11 0 22 0 9 0 9 18 9 20 19 1 14 1 13 0 5 20 9 11 19 12 9 18 0 1 14 0 13 9 4 15 8 0 5 20 8 19 0 5 20 9 8 19 1 14 0 19 | s nashite shte hodim na rilskite manastiri i v kolata ima miasto za oshte edin chovek iskash li
9 | 85 <br/> 5 0 9 12 0 15 14 4 21 18 20 0 5 8 3 5 22 0 9 3 15 18 21 0 1 14 0 1 12 9 4 15 8 0 5 8 19 5 2 0 9 1 13 0 9 20 0 15 14 1 9 16 0 1 14 0 1 9 18 9 22 19 0 1 4 0 1 8 3 21 1 14 0 5 19 0 1 4 0 1 9 12 19 9 13 | mislia da se naucha da sviria na piano ti mai beshe hodila na uroci veche trudno li e
10 | 21 <br/> 15 14 4 5 1 26 0 13 5 4 1 9 0 1 4 0 5 4 9 1 8 | haide da iadem zaedno
