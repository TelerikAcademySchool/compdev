№ | Вход | Изход
--- | --- | --- 
1 | 12 45 98 67 23 5 99 2 7 0 22 8 12 24 100 23 54 11 29 30 <br/> 30 1 23 45 99 34 78 32 45 34 6 78 9 4 34 23 34 3 19 23 | Genadi: 12 tochki <br/> Gerasim: 7 tochki <br/> Nikoi ne pecheli
2 | 45 21 63 6 37 64 1 64 82 65 60 96 24 25 90 97 4 93 73 13 <br/> 45 61 30 69 7 64 1 91 54 92 14 6 40 9 2 66 3 4 96 90 | Genadi: 10 tochki <br/> Gerasim: 7 tochki <br/> Genadi e pobeditel
3 | 26 61 38 16 33 89 2 7 35 41 6 68 25 49 5 92 29 54 85 43 <br/> 67 61 29 86 82 50 8 55 47 54 21 24 4 77 59 60 66 16 25 83 | Genadi: 7 tochki <br/> Gerasim: 12 tochki <br/> Nikoi ne pecheli
4 | 83 93 48 60 91 5 80 90 49 59 28 69 23 6 46 100 9 14 3 19 <br/> 41 50 81 61 33 69 17 10 19 9 88 47 40 13 67 39 29 3 64 76 | Genadi: 10 tochki <br/> Gerasim: 10 tochki <br/> I dvamata pechelite
5 | 11 64 0 18 39 70 94 60 5 90 28 8 52 20 61 75 97 98 65 33 <br/> 75 90 3 15 82 72 47 49 78 51 6 45 52 30 79 42 84 89 58 60 | Genadi: 9 tochki <br/> Gerasim: 10 tochki <br/> Gerasim e pobeditel
6 | 100 100 100 100 100 100 100 100 100 100 100 100 100 100 100 100 100 100 100 100  <br/> 100 100 100 100 100 100 100 100 100 100 100 100 100 100 100 100 100 100 100 100 | Genadi: 0 tochki <br/> Gerasim: 0 tochki <br/> Nikoi ne pecheli
7 | 100 100 100 100 100 100 100 100 100 100 100 100 100 100 100 100 100 100 100 100  <br/> 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 | Genadi: 20 tochki <br/> Gerasim: 0 tochki <br/> Nikoi ne pecheli
8 | 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0  <br/> 100 100 100 100 100 100 100 100 100 100 100 100 100 100 100 100 100 100 100 100 | Genadi: 0 tochki <br/> Gerasim: 20 tochki <br/> Nikoi ne pecheli
9 | 50 50 50 50 50 50 50 50 50 50 25 25 25 25 25 25 25 25 25 25  <br/> 25 25 25 25 25 25 25 25 25 25 50 50 50 50 50 50 50 50 50 50 | Genadi: 10 tochki <br/> Gerasim: 10 tochki <br/> I dvamata pechelite
10 | 53 79 50 18 64 24 68 38 61 20 25 69 35 22 91 23 5 39 7 76 <br/> 71 79 1 95 64 41 47 77 62 35 91 82 11 34 19 28 58 57 50 69 | Genadi: 5 tochki <br/> Gerasim: 13 tochki <br/> Nikoi ne pecheli
