#include <iostream>
using namespace std;

int main(){
    int n=20;
    int win=10;
    int p1=0, p2=0;
    int arr1[n];

    for (int i=0; i<n; i++){
        cin>>arr1[i];
    }

    for (int i=0; i<n; i++){
        int num;
        cin>>num;

        if(arr1[i]>num){
            p1++;
        }

        if(num>arr1[i]){
            p2++;
        }
    }

    cout<<"Genadi: "<<p1<<" tochki"<<endl;
    cout<<"Gerasim: "<<p2<<" tochki"<<endl;

    if(p1==win && p2==win){
        cout<<"I dvamata pechelite";
    } else if(p1==win){
        cout<<"Genadi e pobeditel";
    } else if(p2==win){
        cout<<"Gerasim e pobeditel";
    } else {
        cout<<"Nikoi ne pecheli";
    }

    return 0;
}
