В Масивландия хората силно почитат масивите от числа. Те правят различни ритуали в зависимост от това как са подредени числата в даден масив. Най-специалният ритуал се нарича "Палиндромос Масивос" и се изпълнява само и единствено, когато хората срещнат масив палиндром - масив, чийто елементи са симетрични (първия е равен на последния, втория на предпоследния и т.н.). Понякога тези масиви може да съдържат твърде много числа и става много сложно за жителите на Масивландия да определят дали са палиндроми. Затова те ви назначават, за да напишете програма, която да го прави вместо тях като при даден масив директно ги информира дали да правят ритуала или не.

## Вход
* На първия ред се въвежда броя числа в масива.
* На втория ред, отделени с интервал, се въвеждат числата.

## Изход
Извежда се ``Palindromos Masivos!``, ако масивът е палиндром и ``Nishto interesno`` в противен случай.

## Ограничения
* ``1`` <= ``брой`` <= ``1000``
* ``0`` <= ``число`` <= ``100``

## Пример

### Вход: 
```
5
2 9 3 9 2
```
### Изход: 
```
Palindromos Masivos!
```

### Вход: 
```
12
23 34 56 45 3432 54 54 3432 45 56 34 23
```
### Изход: 
```
Palindromos Masivos!
```

### Вход: 
```
8
1 2 3 4 4 8 9 1
```
### Изход: 
```
Nishto interesno
```
