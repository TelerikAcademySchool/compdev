#include <iostream>
using namespace std;
int main()
{
    int n;
    cin>>n;
    int arr[n];
    float sum = 0;
    //tezi stoinosti sa vzeti ot ogranicheniata v zadachata
    int minnum = 1000000;
    int maxnum = 0;

    for (int i=0; i<n; i++){
        cin>>arr[i];

        if(arr[i]>maxnum){
            maxnum=arr[i];
        }

        if(arr[i]<minnum){
            minnum=arr[i];
        }

        sum+=arr[i];
    }

    for (int i=0; i<n; i++){
        cout<<arr[i]-minnum<<" ";
    }

    cout<<"\n"<<minnum<<" "<<maxnum<<" "<<sum/n;

    return 0;
}
