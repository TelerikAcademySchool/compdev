Напишете програма, която въвежда стойностите на целите числа a, b, x и y и извежда резултата на израза: ``x*(y+2)*(a+b)``.

## Вход
На четири реда се въвеждат a, b, x и y.

## Изход
Резултатът от дадения израз.

## Ограничения
* ``0`` <= ``a`` <= ``10000``
* ``0`` <= ``b`` <= ``10000``
* ``0`` <= ``x`` <= ``10000``
* ``0`` <= ``y`` <= ``10000``

## Пример

### Вход:
```
1
2
3
4
```
### Изход: 
```
54
```

### Вход:
```
5
12
34
7
```
### Изход: 
```
5202
```