№ | Вход | Изход
--- | --- | --- 
1 | 10 <br/> 20 <br/> 15 | 45
2 | 2300 <br/> 1350 <br/> 3473 | 7123
3 | 5023 <br/> 1234 <br/> 4332 | 10589
4 | 0 <br/> 12 <br/> 34 | 46
5 | 234324544 <br/> 343332342 <br/> 954343230 | 1532000116
6 | 2000000000 <br/> 2000000000 <br/> 2000000000 | 6000000000
