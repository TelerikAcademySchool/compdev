#include <iostream>
#include <cmath>
using namespace std;

int main()
{
    int n, edinici, desetici, stotici, hiliadi, suma;
    cin>>n;
    edinici = n%10;
    desetici = (n % 100) / 10;
    stotici = (n % 1000) / 100;
    hiliadi = n/1000;
    suma = edinici + desetici + stotici + hiliadi;
    cout<<suma<<endl;
    return 0;
}
