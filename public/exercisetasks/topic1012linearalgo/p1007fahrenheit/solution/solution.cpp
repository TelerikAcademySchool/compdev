#include <iostream>
#include <math.h>

using namespace std;

int main()
{
    float c, f;
    float ctof, ftoc;
    cin>>c>>f;
    ctof = c*1.8 + 32;
    ftoc = (f-32)/1.8;
    cout<<ceil(ctof)<<endl;
    cout<<ceil(ftoc)<<endl;
    return 0;
}
