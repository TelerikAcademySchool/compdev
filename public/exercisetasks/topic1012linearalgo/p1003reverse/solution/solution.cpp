#include <iostream>
#include <cmath>
using namespace std;

int main()
{
    int n, edinici, desetici, stotici, rev;
    cin>>n;
    edinici = n%10;
    desetici = (n % 100) / 10;
    stotici = n/100;
    rev = 100*edinici + 10*desetici + stotici;
    cout<<rev<<endl;
    return 0;
}
