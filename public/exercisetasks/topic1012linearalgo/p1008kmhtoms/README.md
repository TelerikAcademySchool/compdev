Напишете програма, която приема скорост, измерена в километри в час и я превръща в метри за секунда.

## Вход
Число, представляващо скорост в км/ч.

## Изход
Еквивалентната скорост в м/с.

## Ограничения
* ``0`` <= ``скорост в км/ч`` <= ``10 000``

## Пример

### Вход:
```
60
```
### Изход: 
```
16.6667
```