В компанията "Лола Лока" искат да групират всички служители по двойки, така че да обменят опит. Условието за групиране е хората в една двойка да не са връстници (под връстници в компанията разбират хора на еднаква възраст). Помогнете на "Лола Лока" като направите програма, която приема две цели числа - годините на двама човека и извежда 1, ако може да се групират (тоест не са връстници) и 0, ако не може да се групират (връстници са - възрастите им са еднакви).

## Вход
На входа се въвеждат две числа, разделени с интервал - годините на единия човек и годините на другия човек.

## Изход
1, ако хората може да се групират в една двойка (не са връстници) и 0, ако не може да се групират (връстници са).  

## Ограничения
* ``0`` <= ``възраст`` <= ``150``

## Пример

### Вход:
```
19 19
```
### Изход: 
```
0
```
  
### Вход:
```
18 23
```
### Изход: 
```
1
```