Напишете програма, която на входа получава две числа и извежда 1, ако са равни и 0, ако не са равни.

## Вход
На входа се въвеждат две числа, разделени с интервал.

## Изход
1 или 0 в зависимост дали числата са равни или не.  

## Ограничения
* ``-100 000 000.00`` <= ``число`` <= ``99 999 999.99``

## Пример

### Вход:
```
3245 3245
```
### Изход: 
```
1
```
  
### Вход:
```
856 123
```
### Изход: 
```
0
```