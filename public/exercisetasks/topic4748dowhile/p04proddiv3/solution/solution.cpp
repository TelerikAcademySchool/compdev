#include <iostream>
using namespace std;
int main()
{
    long long num,temp;
    long long prod = 1;
    bool alldiv3 = true;

    cin>>num;
    do
    {
        temp=num%10;
        if(temp%3!=0){
           prod*=temp;
           alldiv3 = false;
        }

        num=num/10;
    }
    while(num!=0);

    if(alldiv3){
        cout<<-1<<endl;
    } else {
        cout<<prod<<endl;
    }

    return 0;
}
