#include <iostream>
using namespace std;
int main()
{
    int evencount = 0;
    int oddcount = 0;
    int zerocount = 0;
    int num;

    do {
        cin>>num;
        if(num%2==0){
            evencount++;
        } else {
            oddcount++;
        }

        if(num==0){
            zerocount++;
        }
    }
    while (zerocount<3);

    cout<<evencount<<endl;
    cout<<oddcount<<endl;

    return 0;
}
