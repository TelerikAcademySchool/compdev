№ | Вход | Изход
--- | --- | --- 
1 | 2427294 2 | Cifrata 2 se sreshta 3 puti v chisloto 2427294.
2 | 3245 1 | Chisloto 3245 ne sudurja cifrata 1!
3 | 2344547542 0 | Chisloto 2344547542 ne sudurja cifrata 0!
4 | 34975 5 | Cifrata 5 se sreshta 1 puti v chisloto 34975.
5 | 3457094 4 | Cifrata 4 se sreshta 2 puti v chisloto 3457094.
6 | 421561781 1 | Cifrata 1 se sreshta 3 puti v chisloto 421561781.
7 | 0 8 | Chisloto 0 ne sudurja cifrata 8!
8 | 888888888888 8 | Cifrata 8 se sreshta 12 puti v chisloto 888888888888.
9 | 999999999999 3 | Chisloto 999999999999 ne sudurja cifrata 3!
10 | 1000000000000 0 | Cifrata 0 se sreshta 12 puti v chisloto 1000000000000.
11 | 7 7 | Cifrata 7 se sreshta 1 puti v chisloto 7.
12 | 35 3 | Cifrata 3 se sreshta 1 puti v chisloto 35.
13 | 0 0 | Cifrata 0 se sreshta 1 puti v chisloto 0.
