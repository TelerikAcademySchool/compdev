#include <iostream>
using namespace std;
int main()
{
    long long num,temp,orgnum;
    int digit;
    int br = 0;

    cin>>num>>digit;
    orgnum = num;

    do
    {
        temp=num%10;
        if(temp==digit){
           br++;
        }

        num=num/10;
    }
    while(num!=0);

    if(br==0){
        cout<<"Chisloto "<<orgnum<<" ne sudurja cifrata "<<digit<<"!"<<endl;
    } else {
        cout<<"Cifrata "<<digit<<" se sreshta "<<br<<" puti v chisloto "<<orgnum<<"."<<endl;
    }

    return 0;
}
