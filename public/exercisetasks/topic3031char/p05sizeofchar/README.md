Да се състави програма, която извежда размера в брой байтове на променлива от тип char.

## Вход
Няма входни данни.

## Изход
Извежда се размера в брой байтове на променлива от тип char.