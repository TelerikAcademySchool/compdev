Да се състави програма, която прочита от клавиатурата ASCII код и извежда съответстващия му символ.

## Вход
На входа се въвежда число.

## Изход
Извежда се съответстващия символ.

## Ограничения
* ``33`` <= ``число`` <= ``126``

## Пример

### Вход:
```
33
```
### Изход: 
```
!
```