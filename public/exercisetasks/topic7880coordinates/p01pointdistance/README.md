Направете програма, която въвежда от клавиатурата координатите на две точки с равни абсциси и извежда разстоянието между тях. Ако двете точки не са с равни абсциси, да се изведе съобщение ``Abscisite triabva da sa ravni!``.

## Вход
На първия ред се въвеждат координатите на едната точка.  
На втория ред - на другата точка.

## Изход
Извежда се разстоянието между дадените точки или горепосоченото съобщение за неравни абсциси.

## Ограничения
* ``0`` <= ``x1`` <= ``1000``
* ``0`` <= ``y1`` <= ``1000``
* ``0`` <= ``x2`` <= ``1000``
* ``0`` <= ``y2`` <= ``1000``

## Пример

### Вход: 
```
32 102
32 507
```
### Изход: 
```
405
```

### Вход: 
```
5 23
43 102
```
### Изход: 
```
Abscisite triabva da sa ravni!
```
