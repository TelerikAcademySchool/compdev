#include <iostream>
#include <math.h>
using namespace std;

int main() {
    int x1, x2, y1, y2, a, b;
    cin>>x1>>y1>>x2>>y2;
    a = fabs(x1-x2);
    b = fabs(y1-y2);

    cout<<a*b<<" "<<2*a+2*b<<endl;

    return 0;
}
