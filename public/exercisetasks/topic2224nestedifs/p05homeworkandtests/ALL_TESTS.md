№ | Вход | Изход
--- | --- | --- 
1 | 3 5 <br/> 2 | 5.5.2022 <br/> ima kontrolno
2 | 23 8 <br/> 11 | 3.9.2022 <br/> ima kontrolno
3 | 30 12 <br/> 21 | 20.1.2023 <br/> niama kontrolno
4 | 19 12 <br/> 25 | 13.1.2023 <br/> ima kontrolno
5 | 1 10 <br/> 1 | 2.10.2022 <br/> niama kontrolno
6 | 23 2 <br/> 16 | 11.3.2022 <br/> ima kontrolno
7 | 1 4 <br/> 14 | 15.4.2022 <br/> niama kontrolno
8 | 30 3 <br/> 5 | 4.4.2022 <br/> niama kontrolno
9 | 10 5 <br/> 25 | 4.6.2022 <br/> niama kontrolno
10 | 23 6 <br/> 16 | 9.7.2022 <br/> ima kontrolno
11 | 23 7 <br/> 11 | 3.8.2022 <br/> ima kontrolno
12 | 14 11 <br/> 5 | 19.11.2022 <br/> ima kontrolno
13 | 12 12 <br/> 13 | 25.12.2022 <br/> ima kontrolno
