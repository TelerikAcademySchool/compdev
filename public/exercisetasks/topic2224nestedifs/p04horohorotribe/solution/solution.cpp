#include <iostream>
using namespace std;
int main() {
	int n, edinici, desetici, stotici, hiliadni, sum, prod, maxdigit, mindigit;
	cin >> n;
	edinici = n % 10;
	desetici = (n%100) / 10;
	stotici = (n%1000) / 100;
	hiliadni = n / 1000;
	//cout << edinici << " " << desetici << " " << stotici << " " << hiliadni;

    sum = edinici + desetici + stotici + hiliadni;
    prod = edinici * desetici * stotici * hiliadni;

    if(sum>prod){
        maxdigit = edinici;
        if(desetici>maxdigit){
            maxdigit = desetici;
        }

        if(stotici>maxdigit){
            maxdigit = stotici;
        }

        if(hiliadni>maxdigit){
            maxdigit = hiliadni;
        }

        cout << maxdigit;
    } else {
        mindigit = edinici;
        if(desetici<mindigit){
            mindigit = desetici;
        }

        if(stotici<mindigit){
            mindigit = stotici;
        }

        if(hiliadni<mindigit){
            mindigit = hiliadni;
        }

        cout << mindigit;
    }

	return 0;
}
