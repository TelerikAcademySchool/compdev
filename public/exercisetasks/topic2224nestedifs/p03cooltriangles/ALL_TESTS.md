№ | Вход | Изход
--- | --- | --- 
1 | 3 4 5 | Ne tolkova gotin triugulnik
2 | 11 45 12 | Ne sushtestvuva takuv triugulnik!
3 | 3 4 -6 | Nekorektni danni!
4 | 30 40 50 | Gotin triugulnik
5 | 0 2324 54434 | Nekorektni danni!
6 | 10 45 45 | Ne tolkova gotin triugulnik
7 | 10000000000 10000000000 10000000000 | Gotin triugulnik
8 | 12 4 38 | Ne sushtestvuva takuv triugulnik!
9 | 123 12 8 | Ne sushtestvuva takuv triugulnik!
10 | 0 23 45 | Nekorektni danni!
11 | 435435 0 2323 | Nekorektni danni!
