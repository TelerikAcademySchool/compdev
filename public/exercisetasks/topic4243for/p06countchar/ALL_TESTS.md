№ | Вход | Изход
--- | --- | --- 
1 | 10 A <br/> d s S k K a l k a A | 3
2 | 3 d <br/> w k a | 0
3 | 20 l <br/> d A l s a L l o d w q L L s l w q L e l | 8
4 | 1 w <br/> g | 0
5 | 1 g <br/> G | 1
6 | 50 b <br/> b B b B b B b B b B b B b B b B b B b B b B b B b B b B b B b B b B b B b B b B b B b B b B b B b B | 50
7 | 70 k <br/> a A a A a A a A a A a A a A a A a A a A a A a A a A a A a A a A a A a A a A a A a A a A a A a A a A a A a A a A a A a A a A a A a A a A a A a A a A a A a A a A a A a A | 0
8 | 7 d <br/> D D D D D D D | 7
9 | 8 Z <br/> Z z d D z Z a d | 4
10 | 100 X <br/> X x x x X x x x X X X x x x X x x x X X X x x x X x x x X X X x x x X x x x X X X x x x X x x x X X X x x x X x x x X X X x x x X x x x X X X x x x X x x x X X X x x x X x x x X X X x x x X x x x X X | 100
