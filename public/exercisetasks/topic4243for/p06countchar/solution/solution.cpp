#include <iostream>
using namespace std;
int main()
{
    int n, br=0;
    char ch,invch;
    cin>>n>>ch;
    int numch=ch;

    if (numch>=65 && numch<=90){
        invch=(char)(numch+32);
    } else {
        invch=(char)(numch-32);
    }

    for(int i=0;i<n;i++){
        char letter;
        cin>>letter;
        if(letter==ch || letter==invch){
            br++;
        }
    }

    cout<<br;

    return 0;
}
