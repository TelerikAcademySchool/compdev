№ | Вход | Изход
--- | --- | --- 
1 | 10 <br/> 4 7 8 2 1 9 2 10 3 5 | 1209600
2 | 5 <br/> 10 3 0 7 1 | Eeeee razvaliash mi goliamoto proizvedenie!
3 | 1 <br/> 5 | 5
4 | 2 <br/> 8 9 | 72
5 | 3 <br/> 2 7 9 | 126
6 | 4 <br/> 10 1 7 3 | 210
7 | 6 <br/> 1 3 5 7 9 2 | 1890
8 | 7 <br/> 1 1 1 1 1 1 1 | 1
9 | 8 <br/> 2 0 8 4 0 2 0 4 | Eeeee razvaliash mi goliamoto proizvedenie!
10 | 10 <br/> 10 10 10 10 10 10 10 10 10 10 | 10000000000
