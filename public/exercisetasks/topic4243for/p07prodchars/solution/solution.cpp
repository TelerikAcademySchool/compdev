#include <iostream>
using namespace std;
int main()
{
    int n;
    long long p=1;
    bool nonums = true;
    cin>>n;

    for(int i=0;i<n;i++){
        char letter;
        cin>>letter;
        int code = letter;
        if (code>=48 && code<=57){
            int num = letter-'0';
            if(num!=0){
               p*=num;
               nonums = false;
            }
        }
    }

    if(nonums){
        cout<<0;
    }else{
        cout<<p;
    }


    return 0;
}
