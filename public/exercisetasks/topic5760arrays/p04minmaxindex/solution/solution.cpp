#include <iostream>
using namespace std;

int main(){
    int n, minnum, maxnum, minindex, maxindex;
    cin>>n;
    int arr[n];

    for (int i=0; i<n; i++){
        cin>>arr[i];
    }

    minnum = arr[0];
    maxnum = arr[0];
    minindex = 0;
    maxindex = 0;

    for (int i=0; i<n; i++){
        if(arr[i]<minnum){
            minnum=arr[i];
            minindex=i;
        }

        if(arr[i]>maxnum){
            maxnum=arr[i];
            maxindex=i;
        }
    }

    //cout<<minnum<<endl;
    //cout<<maxnum<<endl;
    cout<<minindex*maxindex<<endl;

    return 0;
}
