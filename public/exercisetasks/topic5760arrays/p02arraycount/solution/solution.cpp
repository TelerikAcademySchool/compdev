#include <iostream>
using namespace std;

int main(){
    int n, k, pos, neg, zero, knum;
    cin>>n;
    int arr[n];
    pos=0;
    neg=0;
    zero=0;
    knum=0;

    for (int i=0; i<n; i++){
        cin>>arr[i];
    }

    cin>>k;

    for (int i=0; i<n; i++){
        if(arr[i]==k){
            knum++;
        }

        if(arr[i]>0){
            pos++;
        }

        if(arr[i]<0){
            neg++;
        }

        if(arr[i]==0){
            zero++;
        }
    }

    cout<<knum<<endl;
    cout<<pos<<endl;
    cout<<neg<<endl;
    cout<<zero<<endl;

    return 0;
}
