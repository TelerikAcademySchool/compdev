#include <iostream>
using namespace std;

int main(){
    int n, k, sum, sumk;
    long long prod;
    cin>>n;
    int arr[n];
    prod=1;
    sum=0;
    sumk=0;

    for (int i=0; i<n; i++){
        cin>>arr[i];
    }

    cin>>k;

    for (int i=0; i<n; i++){
        if(arr[i]%k==0){
            sumk+=arr[i];
        }

        sum+=arr[i];
        prod*=arr[i];
    }

    cout<<sumk<<endl;
    cout<<prod<<endl;
    cout<<(double)sum/n<<endl;

    return 0;
}
