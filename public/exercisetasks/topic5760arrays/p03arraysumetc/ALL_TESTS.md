№ | Вход | Изход
--- | --- | --- 
1 | 8 <br/> 2 5 1 8 0 4 6 4 <br/> 4 | 16 <br/> 0 <br/> 3.75
2 | 10 <br/> 1 2 3 4 5 6 7 8 9 10 <br/> 2 | 30 <br/> 3628800 <br/> 5.5
3 | 9 <br/> 9 3 7 6 5 5 4 5 5 <br/> 5 | 20 <br/> 2835000 <br/> 5.44444
4 | 10 <br/> 0 0 0 0 0 0 0 0 0 0  <br/> 3 | 0 <br/> 0 <br/> 0
5 | 3 <br/> 9 9 9 <br/> 3 | 27 <br/> 729 <br/> 9
6 | 5 <br/> 10 10 2 10 8 <br/> 2 | 40 <br/> 16000 <br/> 8
7 | 6 <br/> 2 8 7 4 5 8 <br/> 3 | 0 <br/> 17920 <br/> 5.66667
8 | 10 <br/> 10 10 10 10 10 10 10 10 10 10 <br/> 5 | 100 <br/> 10000000000 <br/> 10
9 | 1 <br/> 5 <br/> 2 | 0 <br/> 5 <br/> 5
10 | 10 <br/> 9 9 9 9 9 9 9 9 9 9 <br/> 2 | 0 <br/> 3486784401 <br/> 9
